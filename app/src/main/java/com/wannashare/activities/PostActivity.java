package com.wannashare.activities;



import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.google.android.gms.maps.model.LatLng;
import com.wannashare.R;
import com.wannashare.adapters.ImagesGalleryAdapter;
import com.wannashare.datahelper.async.post.SaveCreatedPostData;
import com.wannashare.datahelper.user.UserDataHelper;
import com.wannashare.localdatastore.models.NearbyPost;
import com.wannashare.localdatastore.models.Post;
import com.wannashare.utils.ActivityEnum;
import com.wannashare.utils.Constants;
import com.wannashare.utils.Utils;

import java.sql.SQLException;

/**
 * Created by chaitanya on 6/24/15.
 */
public class PostActivity extends ActionBarBaseActivity {

    private EditText postEditText;
    private TextView characterCountTextView;
    private Button postButton;
    private int maxCharacterCount = 140;
    private String[] imagesPath = null;
    private LinearLayout lnrImages;
    private ViewPager gallery;
    UserDataHelper userDataHelper;
    LatLng currentLocation;
    private Bitmap yourbitmap;
    private Bitmap resized;
    private final int PICK_IMAGE_MULTIPLE =1;

    public PostActivity() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        renderActionBarView(ActivityEnum.POST, false, false);
        postEditText = (EditText) findViewById(R.id.post_edittext);
      //  lnrImages = (LinearLayout)findViewById(R.id.lnrImages);
        characterCountTextView = (TextView) findViewById(R.id.character_count_textview);
        gallery = (ViewPager) findViewById(R.id.gallery_item);
        try {
            userDataHelper = new UserDataHelper();
        } catch (SQLException e) {
            Log.e(Constants.APP_TAG,e.getMessage());
        }
        imagesPath = new String[50];

        setUpActionBar();


        postEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                updatePostButtonState();
                updateCharacterCountTextViewText();
            }
        });


        postButton = (Button) findViewById(R.id.post_button);
        postButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    post();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
        });

        updatePostButtonState();
        updateCharacterCountTextViewText();



    }

    @Override
    protected void onResume() {


        gallery.setAdapter(new ImagesGalleryAdapter(this, imagesPath));
        super.onResume();
    }


    private void post () throws SQLException {
        String text = postEditText.getText().toString().trim();
        String[] latLngArray = localCacheAccessor.getFromCache("currentLocation").split(",");
       // LatLng geoPoint = mapService.getCurrentLocation();
       // Location loc = null;
        //locationDataServiceDelete.getLastLocation();

        // Set up a progress dialog
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.progress_post));
        dialog.show();


        Post post = new Post();
        post.setPost(text);

        post.setLatitude(Double.parseDouble(latLngArray[0]));
        post.setLongitude(Double.parseDouble(latLngArray[1]));
        post.setUserid(AccessToken.getCurrentAccessToken().getUserId());
        new SaveCreatedPostData(post,imagesPath).execute();

        dialog.dismiss();

        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(postEditText.getWindowToken(), 0);
        getFragmentManager().popBackStack();
        finish();
        // Intent intent = new Intent(getActivity(),MainActivity.class);
       // startActivity(intent);
       /* FragmentTransaction t = getActivity().getFragmentManager().beginTransaction();
        Fragment mFrag = new MapFragment();
        t.replace(R.id.frame_container, mFrag);
        t.commit(); */


    }

    private String getPostEditTextText () {
        return postEditText.getText().toString().trim();
    }

    private void updatePostButtonState () {
        int length = getPostEditTextText().length();
        boolean enabled = length > 0 && length < maxCharacterCount;
        postButton.setEnabled(enabled);
    }

    private void updateCharacterCountTextViewText () {
        String characterCountString = String.format("%d/%d", postEditText.length(), maxCharacterCount);
        characterCountTextView.setText(characterCountString);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        StringBuilder sb=null;
        if (resultCode == RESULT_OK) {
            String[] currentImagesPath = data.getStringExtra("data").split("\\|");

            int imagesPathLength = Utils.getCurrentLength(imagesPath);

                for(int i=0;i<currentImagesPath.length;i++){
                    imagesPath[imagesPathLength+i]=currentImagesPath[i];

                }


        }


    }




    private void setUpActionBar(){



        actionBar.setTitle("Add Share");

        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

        actionBar.setDisplayOptions(actionBar.getDisplayOptions()|ActionBar.DISPLAY_SHOW_CUSTOM);
        ImageView imageView=new ImageView(actionBar.getThemedContext());
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        imageView.setImageResource(R.drawable.ic_attachment_black_24dp);
        ActionBar.LayoutParams layoutParams=new ActionBar.LayoutParams(125,125,Gravity.RIGHT
        |Gravity.CENTER_VERTICAL);
        layoutParams.rightMargin=40;
        imageView.setLayoutParams(layoutParams);
        actionBar.setCustomView(imageView);

        imageView.setOnClickListener(new View.OnClickListener(

        ) {
            /*
 * Called when a view has been clicked.
 *
 * @param v The view that was clicked.
 */
            @Override
            public void onClick(View v) {
                if (postEditText.getText().toString() != null) {
                    localCacheAccessor.persistToCache("editText", postEditText.getText().toString());

                }
                Intent intent = new Intent(PostActivity.this,CustomPhotoGallery.class);

                startActivityForResult(intent,1);

            }
        });
    }
        }



