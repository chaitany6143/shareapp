package com.wannashare.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;


/**
 * Created by chaitanya on 5/17/15.
 */
public class IsLoggedInHandler extends Activity {

    AccessTokenTracker accessTokenTracker;

    public IsLoggedInHandler() {
    }

    /*This class checks if the user is already logged in or not.If logged in,it redirects to the
    * Home page,else redirects to the Login page activity. */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

      /*  if (AccessToken.getCurrentAccessToken()!=null) {

            accessTokenTracker = new AccessTokenTracker() {
                @Override
                protected void onCurrentAccessTokenChanged(
                        AccessToken oldAccessToken,
                        AccessToken currentAccessToken) {

                    Gson gson = new Gson();

                    UserDataHelper userService = ((MainApplication)ApplicationContextHolder.getApplicationContext()).getUserService();
                    String sessionJson = userService.getFromLocalCache("session");
                    Session session = gson.fromJson(sessionJson, Session.class);
                    session.setToken(currentAccessToken.getToken());

                    userService.saveToLocalCache("session", gson.toJson(session));


                    try {
                        userService.saveSessionToServer(currentAccessToken.getUserId(),session);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            };*/

            if(AccessToken.getCurrentAccessToken()!=null && !AccessToken.getCurrentAccessToken().isExpired())
               startActivity(new Intent(this, MainActivity.class));
            else
                startActivity(new Intent(this, LoginActivity.class));

            ///code to validate the token

      /*  }
        else
            startActivity(new Intent(this, LoginActivity.class));*/

    }

}
