package com.wannashare.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;

import com.facebook.CallbackManager;
import com.wannashare.R;
import com.wannashare.datahelper.login.LoginHelperFactory;
import com.wannashare.datahelper.login.ILoginHelper;
import com.wannashare.utils.ActivityEnum;
import com.wannashare.utils.Constants;
import com.wannashare.wrappers.ContextStore;


/**
 * Created by chaitanya on 5/19/15.
 */
public class LoginActivity extends Activity {

    CallbackManager  callbackManager;

    ILoginHelper fbILoginHelper;

   // LocationDataServiceDelete locationDataServiceDelete = ((MainApplication)ContextStore.getApplicationContext()).getLocationDataService();

    /**
     * Called when the activity is starting.  This is where most initialization
     * should go: calling {@link #setContentView(int)} to inflate the
     * activity's UI, using {@link #findViewById} to programmatically interact
     * with widgets in the UI, calling
     * {@link #managedQuery(Uri, String[], String, String[], String)} to retrieve
     * cursors for data being displayed, etc.
     * <p/>
     * <p>You can call {@link #finish} from within this function, in
     * which case onDestroy() will be immediately called without any of the rest
     * of the activity lifecycle ({@link #onStart}, {@link #onResume},
     * {@link #onPause}, etc) executing.
     * <p/>
     * <p><em>Derived classes must call through to the super class's
     * implementation of this method.  If they do not, an exception will be
     * thrown.</em></p>
     *
     * @param savedInstanceState If the activity is being re-initialized after
     *                           previously being shut down then this Bundle contains the data it most
     *                           recently supplied in {@link #onSaveInstanceState}.  <b><i>Note: Otherwise it is null.</i></b>
     * @see #onStart
     * @see #onSaveInstanceState
     * @see #onRestoreInstanceState
     * @see #onPostCreate
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
       // FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_login);
        ContextStore.getInstance().setContext(ActivityEnum.LOGIN, this);

        fbILoginHelper =  LoginHelperFactory.getHandler(Constants.LOGIN_FACEBOOK, ActivityEnum.LOGIN);
        fbILoginHelper.handleLogin();

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
      fbILoginHelper.handleOnActivityResult(requestCode, resultCode, data);

    }
    }

