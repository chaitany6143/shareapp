package com.wannashare.activities;

import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;
import com.wannashare.R;
import com.wannashare.adapters.TabsPagerAdapter;
import com.wannashare.application.MainApplication;
import com.wannashare.fragments.MapFragment;
import com.wannashare.utils.ActivityEnum;
import com.wannashare.utils.Constants;
import com.wannashare.utils.FragmentEnum;
import com.wannashare.wrappers.ContextStore;

public class MainActivity extends ActionBarBaseActivity implements
		ActionBar.TabListener  {




	public CharSequence getmTitle() {
		return mTitle;
	}
	private CharSequence mTitle;
	private ViewPager viewPager;
	private TabsPagerAdapter mAdapter;
	boolean actionBarRendered;
	boolean tabsRendered;



	@Override
	protected void onCreate(Bundle savedInstanceState) {


		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ContextStore.getInstance().setContext(ActivityEnum.HOME, this);


		mTitle=getTitle();
		actionBarRendered = renderActionBarView(ActivityEnum.HOME, true, false);




		tabsRendered = renderTabs();
		enableGps(this);


	}

	/**
	 * Dispatch onResume() to fragments.  Note that for better inter-operation
	 * with older versions of the platform, at the point of this call the
	 * fragments attached to the activity are <em>not</em> resumed.  This means
	 * that in some cases the previous state may still be saved, not allowing
	 * fragment transactions that modify the state.  To correctly interact
	 * with fragments in their proper state, you should instead override
	 * {@link #onResumeFragments()}.
	 */
	@Override
	protected void onResume() {

		super.onResume();
		if(!actionBarRendered)
			renderActionBarView(ActivityEnum.HOME,true,false);
		if(!tabsRendered)
			renderTabs();
	}

	private boolean renderTabs() {

		viewPager = (ViewPager) findViewById(R.id.pager);
		mAdapter = new TabsPagerAdapter(getSupportFragmentManager());
		viewPager.setAdapter(mAdapter);
		actionBar.setHomeButtonEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Adding Tabs
		for (String tab_name : Constants.tabs) {
			actionBar.addTab(actionBar.newTab().setText(tab_name)
					.setTabListener(this));
		}

		/**
		 * on swiping the viewpager make respective tab selected
		 * */
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				// on changing the page
				// make respected tab selected
				//actionBar.setSelectedNavigationItem(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
return true;
	}


	@Override
	public void onBackPressed() {
		super.onBackPressed();
		//this.moveTaskToBack(true);
	}






	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle action bar actions click
		switch (item.getItemId()) {
			case R.id.action_settings:
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	/* *
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		if(mDrawerLayout!=null && mDrawerList!=null ) {
			boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
			menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		}
		return super.onPrepareOptionsMenu(menu);
	}


	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		actionBar.setTitle(mTitle);
	}



	/**
	 * Called when a tab enters the selected state.
	 *
	 * @param tab The tab that was selected
	 * @param ft  A {@link FragmentTransaction} for queuing fragment operations to execute
	 *            during a tab switch. The previous tab's unselect and this tab's select will be
	 *            executed in a single transaction. This FragmentTransaction does not support
	 */
	@Override
	public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
		viewPager.setCurrentItem(tab.getPosition());
	}

	/**
	 * Called when a tab exits the selected state.
	 *
	 * @param tab The tab that was unselected
	 * @param ft  A {@link FragmentTransaction} for queuing fragment operations to execute
	 *            during a tab switch. This tab's unselect and the newly selected tab's select
	 *            will be executed in a single transaction. This FragmentTransaction does not
	 */
	@Override
	public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

	}

	/**
	 * Called when a tab that is already selected is chosen again by the user.
	 * Some applications may use this action to return to the top level of a category.
	 *
	 * @param tab The tab that was reselected.
	 * @param ft  A {@link FragmentTransaction} for queuing fragment operations to execute
	 *            once this method returns. This FragmentTransaction does not support
	 */
	@Override
	public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
		viewPager.setCurrentItem(tab.getPosition());
	}


}
