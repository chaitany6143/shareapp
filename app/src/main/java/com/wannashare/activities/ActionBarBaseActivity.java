package com.wannashare.activities;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.wannashare.R;
import com.wannashare.adapters.NavDrawerListAdapter;
//import com.wannashare.application.ContextStore;
import com.wannashare.application.MainApplication;
import com.wannashare.fragments.CommunityFragment;
import com.wannashare.fragments.FindPeopleFragment;
import com.wannashare.fragments.MapFragment;
import com.wannashare.fragments.PagesFragment;
import com.wannashare.fragments.PhotosFragment;
import com.wannashare.models.NavDrawerItem;
import com.wannashare.utils.ActivityEnum;
import com.wannashare.wrappers.ContextStore;
import com.wannashare.localdatastore.LocalCacheAccessor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chaitanya on 8/10/15.
 */
public class ActionBarBaseActivity extends ActionBarActivity {



    protected ActionBarDrawerToggle mDrawerToggle;

    protected ActionBar actionBar;

    protected DrawerLayout mDrawerLayout;

    protected CharSequence mDrawerTitle;

    protected String[] navMenuTitles;

    protected TypedArray navMenuIcons;

    protected List<NavDrawerItem> navDrawerItems;

    protected ListView mDrawerList;

    protected NavDrawerListAdapter adapter;

    protected ActivityEnum mActivityEnum;


    protected LocalCacheAccessor localCacheAccessor = ((MainApplication) ContextStore.getApplicationContext()).getLocalCacheAccessor();
    private boolean mEnableHome;
    private  boolean mEnableTabs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

    }

    protected boolean renderActionBarView(ActivityEnum activityEnum, boolean mEnableTabs, boolean isHomeEnable) {

        this.mActivityEnum = activityEnum;
        this.mEnableHome = isHomeEnable;
        this.mEnableTabs = mEnableTabs;

        mDrawerTitle = getTitle();

        actionBar = getSupportActionBar();


       // actionBar.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
       // actionBar.setBackgroundDrawable(new ColorDrawable(R.color.list_background_pressed));

        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

        navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

        addNavigationDrawerItems();



        // setting the nav drawer list adapter
        adapter = new NavDrawerListAdapter(getApplicationContext(),navDrawerItems);
        mDrawerList.setAdapter(adapter);

        // enabling action bar app icon and behaving it as toggle button
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                null, //nav menu toggle icon
                R.string.app_name, // nav drawer open - description for accessibility
                R.string.app_name // nav drawer close - description for accessibility
        ) {
            public void onDrawerClosed(View view) {
                actionBar.setTitle(getTitle());
                // calling onPrepareOptionsMenu() to show action bar icons
               invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                actionBar.setTitle(mDrawerTitle);
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        //mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);


        return true;
    }

    private void addNavigationDrawerItems() {

        navDrawerItems = new ArrayList<NavDrawerItem>();


        //DeleteNavigationDrawer navigationDrawer = new DeleteNavigationDrawer();
        //navigationDrawer.initialize(navMenuTitles,navMenuIcons,mDrawerLayout,mDrawerList,navDrawerItems);
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));

        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
        // Find People
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
        // Photos
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
        // Communities, Will add a counter here
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1), true, "22"));
        // Pages
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
        // What's hot, We  will add a counter here
        //navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1), true, "50+"));

        // load slide menu items


        // Recycle the typed array
        navMenuIcons.recycle();
    }


    /**
     * Slide menu item click listener
     * */
    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // display view for selected nav drawer item
            displayView(position);
        }
    }

    /*
	 * Diplaying fragment view for selected nav drawer list item
	 * */
    protected void displayView(int position) {

        Intent intent;
        // update the main content by replacing fragments
        Fragment fragment = null;
        switch (position) {
            case 1:
                fragment = new MapFragment();
                //fragment = new MapFragment();
                break;
            case 2:
                fragment = new FindPeopleFragment();
                break;
            case 3:
                fragment = new PhotosFragment();
                break;
            case 4:
                fragment = new CommunityFragment();
                break;
            case 5:
                fragment = new PagesFragment();
                break;


            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.pager, fragment).commit();

            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            setTitle(navMenuTitles[position-1]);
            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            // error in creating fragment
            Log.e("HomePageActivity", "Error in creating fragment");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        if(mDrawerToggle!=null)
            mDrawerToggle.syncState();
    }



    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void enableGps(final Context context){
        final boolean result=false;
        LocationManager lm = null;
        boolean gps_enabled = false,network_enabled = false;
        AlertDialog.Builder dialog;
        if(lm==null)
            lm = (LocationManager) ContextStore.getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        try{
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }catch(Exception ex){}
        try{
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }catch(Exception ex){}

        if(!gps_enabled && !network_enabled) {
            dialog = new AlertDialog.Builder(context);
            dialog.setMessage("Gps is not enabled");
            dialog.setPositiveButton("Open settings", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    context.startActivity(myIntent);
                    //get gps

                }
            });
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            dialog.show();

        }

    }








}
