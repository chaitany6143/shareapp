package com.wannashare.activities;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.facebook.AccessToken;
import com.google.android.gms.maps.model.LatLng;
import com.wannashare.R;
import com.wannashare.adapters.CommentListAdapter;

import com.wannashare.datahelper.user.UserDataHelper;

import com.wannashare.localdatastore.models.NearbyPostComment;
import com.wannashare.models.PostCommentResponse;
import com.wannashare.utils.Constants;
import com.wannashare.utils.DaoEnum;
import com.wannashare.utils.MapUtil;

import java.sql.SQLException;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by chaitanya on 6/24/15.
 */
public class CommentActivity extends ActionBarActivity {


    private ListView listView;
    private CommentListAdapter listAdapter;
    private UserDataHelper userDataHelper;
    private Button submitButton;
    private EditText commentBox;
    List<NearbyPostComment> nearbyPostComments;
     String postid;

    public CommentActivity() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {

        try {
            userDataHelper = new UserDataHelper();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        listView = (ListView) findViewById(R.id.comments);

          commentBox = (EditText) findViewById(R.id.commentBox);
        submitButton = (Button) findViewById(R.id.submitComment);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final NearbyPostComment postComment = new NearbyPostComment();
                postComment.setUserid(AccessToken.getCurrentAccessToken().getUserId());
                postComment.setPostid(Integer.parseInt(getIntent().getStringExtra("postid")));
                postComment.setComment(commentBox.getText().toString());
                userDataHelper.savePostComment(postComment, new Callback() {
                    @Override
                    public void success(Object o, Response response) {
                        try {

                            nearbyPostComments = (List<NearbyPostComment>) userDataHelper.getFromLocalDbByField("postid", String.valueOf(postComment.getPostid()), DaoEnum.NEARBYPOSTCOMMENT);

                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                        listAdapter = new CommentListAdapter(CommentActivity.this, nearbyPostComments,userDataHelper);
                        listView.setAdapter(listAdapter);
                        listAdapter.notifyDataSetChanged();

                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(commentBox.getWindowToken(), 0);
                        commentBox.setText("");
                        listAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
            }
        });

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
         postid = getIntent().getStringExtra("postid");

        try {

            nearbyPostComments = (List<NearbyPostComment>) userDataHelper.getFromLocalDbByField("postid",postid, DaoEnum.NEARBYPOSTCOMMENT);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        listAdapter = new CommentListAdapter(this, nearbyPostComments,userDataHelper);
        listView.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();

        
        getSupportActionBar().setTitle("Comments");
        getSupportActionBar().setIcon(
                new ColorDrawable(getResources().getColor(android.R.color.transparent)));




    }

    @Override
    protected void onResume() {


        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.COMMENT_BROADCAST_ACTION);
        registerReceiver(receiver, intentFilter);
    }
    @Override
    public void onPause() {

        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onDestroy();

    }
    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {

                nearbyPostComments = (List<NearbyPostComment>) userDataHelper.getFromLocalDbByField("postid",postid, DaoEnum.NEARBYPOSTCOMMENT);

            } catch (SQLException e) {
                e.printStackTrace();
            }
            
            listAdapter = new CommentListAdapter(CommentActivity.this, nearbyPostComments,userDataHelper);
            listView.setAdapter(listAdapter);
            listAdapter.notifyDataSetChanged();


        }
    };


        }



