package com.wannashare.datahelper;

import com.google.gson.Gson;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.StatementBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.wannashare.application.MainApplication;
import com.wannashare.localdatastore.DaoFactory;
import com.wannashare.utils.DaoEnum;
import com.wannashare.utils.IRestApiClient;
import com.wannashare.wrappers.ContextStore;
import com.wannashare.localdatastore.LocalCacheAccessor;

import java.sql.SQLException;
import java.util.List;

import retrofit.RestAdapter;

/**
 * Created by chaitanya on 7/12/15.
 */

/*Services base class.All the services must extend this class to get access to the restClient,local cache and
* json converter */
public class DataHelper {

    public LocalCacheAccessor localCacheAccessor;
    public RestAdapter restAdapter;



    public IRestApiClient restClient;
    public Gson gson;
    protected DaoFactory daoFactory;
    public IRestApiClient getRestClient() {
        return restClient;
    }

    public DataHelper() throws SQLException {
        localCacheAccessor = ((MainApplication) ContextStore.getApplicationContext()).getLocalCacheAccessor();
        restAdapter = ((MainApplication) ContextStore.getApplicationContext()).getRestAdapter();
        restClient = restAdapter.create(IRestApiClient.class);
        gson = new Gson();
        daoFactory = DaoFactory.getInstance();

    }

    public String getFromLocalCache(String key) {

        return localCacheAccessor.getFromCache(key);

    }

    public void saveToLocalCache(String key, String value) {
        localCacheAccessor.persistToCache(key, value);

    }

    public void savetoLocalDB(Object object, DaoEnum daoEnum) throws SQLException {

        Dao dao = daoFactory.getDao(daoEnum);

        dao.create(object);
    }

    public List<?> getAllFromLocalDb(DaoEnum daoEnum) throws SQLException {
        Dao dao = daoFactory.getDao(daoEnum);
        return dao.queryForAll();
    }

    public List<?> getFromLocalDbByField(String fieldName, String fieldValue, DaoEnum daoEnum) throws SQLException {
        Dao dao = daoFactory.getDao(daoEnum);
        QueryBuilder queryBuilder = dao.queryBuilder();
        queryBuilder.where().eq(fieldName, fieldValue);
        return dao.query(queryBuilder.prepare());
    }

    public void saveToLocalDB(final Object object, final DaoEnum daoEnum, final String fieldName, final String fieldValue) throws SQLException {


                Dao dao = daoFactory.getDao(daoEnum);
                QueryBuilder queryBuilder = dao.queryBuilder();
                try {
                    queryBuilder.where().eq(fieldName, fieldValue);
                    List<?> resultList = null;
                    resultList = dao.query(queryBuilder.prepare());


                    if (resultList.size() == 0)
                        dao.create(object);

                } catch (SQLException e) {
                    e.printStackTrace();
                }





    }
    public boolean  isExistInDB(final Object object, final DaoEnum daoEnum, final String fieldName, final String fieldValue) throws SQLException {


        Dao dao = daoFactory.getDao(daoEnum);
        QueryBuilder queryBuilder = dao.queryBuilder();
        try {
            queryBuilder.where().eq(fieldName, fieldValue);
            List<?> resultList = null;
            resultList = dao.query(queryBuilder.prepare());


            if (resultList.size() == 0)
               return false;

        } catch (SQLException e) {
            e.printStackTrace();
        }


        return true;


    }

    public void updateInLocalDB(String columnname,Object columnValue, final DaoEnum daoEnum, final String fieldName, final String fieldValue) throws SQLException {

        Dao dao = daoFactory.getDao(daoEnum);
        UpdateBuilder updateBuilder = dao.updateBuilder();


            // update the value of your field(s)
           updateBuilder.updateColumnValue(columnname, columnValue);

        updateBuilder.where().eq(fieldName, fieldValue);

        dao.update(updateBuilder.prepare());


    }
    }
