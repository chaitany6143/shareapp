package com.wannashare.datahelper.location;

import android.location.Location;

import com.wannashare.datahelper.async.post.GetCommentsData;
import com.wannashare.localdatastore.models.NearbyPost;
import com.wannashare.localdatastore.models.NearbyPostImage;
import com.wannashare.localdatastore.models.UserPost;
import com.wannashare.datahelper.DataHelper;
import com.wannashare.utils.DaoEnum;
import com.wannashare.utils.ImageManager;

import java.sql.SQLException;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by chaitanya on 9/19/15.
 */
public class LocationDataHelper extends DataHelper {


    public LocationDataHelper() throws SQLException {
    }

    public void fetchAndSaveLocationData(Location loc) {


        restClient.getNearbyPosts(new Callback<List<UserPost>>() {
            @Override
            public void success(List<UserPost> userPosts, Response response) {
                for (UserPost userPost : userPosts) {


                    ImageManager.downloadIfNotExist(userPost);


                    NearbyPost nearbyPost = new NearbyPost();
                    nearbyPost.setUserid(userPost.getUser().getUserid());
                    nearbyPost.setPostid(userPost.getPost().getPostid());
                    nearbyPost.setPost(userPost.getPost().getPost());
                    nearbyPost.setLatitude(userPost.getPost().getLatitude());
                    nearbyPost.setLongitude(userPost.getPost().getLongitude());
                    nearbyPost.setTimestamp(userPost.getPost().getTimestamp());
                    nearbyPost.setTotalcomments(userPost.getTotalcomments());
                    nearbyPost.setTotallikes(userPost.getTotallikes());
                    nearbyPost.setTotaldislikes(userPost.getTotaldisLikes());


                    try {
                        saveToLocalDB(userPost.getUser(), DaoEnum.USER, "userid", userPost.getUser().getUserid());
                        saveToLocalDB(nearbyPost, DaoEnum.NEARBYPOST, "postid", String.valueOf(userPost.getPost().getPostid()));

                        NearbyPostImage nearbyPostImage = new NearbyPostImage();
                        nearbyPostImage.setPostid(userPost.getPost().getPostid());

                        for (String fileLocation : userPost.getThumbnaillocation()) {

                            nearbyPostImage.setPostid(userPost.getPost().getPostid());
                            nearbyPostImage.setThumbnailLocation(fileLocation);
                        }
                        for (String fileLocation : userPost.getActuallocation()) {

                            nearbyPostImage.setPostid(userPost.getPost().getPostid());
                            nearbyPostImage.setActuallocation(fileLocation);

                        }

                        saveToLocalDB(nearbyPostImage, DaoEnum.NEARBYPOSTIMAGE, "postid", String.valueOf(userPost.getPost().getPostid()));
                        new GetCommentsData(userPost.getPost().getPostid(),restClient).execute();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }
}
