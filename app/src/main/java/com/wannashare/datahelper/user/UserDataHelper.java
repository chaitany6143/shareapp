package com.wannashare.datahelper.user;

import android.os.Bundle;
import android.os.StrictMode;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.wannashare.datahelper.async.post.GetCommentsData;
import com.wannashare.localdatastore.models.NearbyPost;
import com.wannashare.localdatastore.models.NearbyPostComment;
import com.wannashare.localdatastore.models.NearbyPostImage;
import com.wannashare.localdatastore.models.Post;
import com.wannashare.localdatastore.models.Session;
import com.wannashare.localdatastore.models.User;
import com.wannashare.localdatastore.models.UserPost;
import com.wannashare.localdatastore.models.UserPostLike;
import com.wannashare.models.FeedItem;
import com.wannashare.datahelper.DataHelper;
import com.wannashare.utils.DaoEnum;
import com.wannashare.utils.ImageManager;
import com.wannashare.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.MultipartTypedOutput;
import retrofit.mime.TypedFile;

/**
 * Created by chaitanya on 6/28/15.
 */
public class UserDataHelper extends DataHelper {



    public UserDataHelper() throws SQLException {

        super();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

    }

    public void saveUserToServer(User user){

    restClient.createUser(user, new Callback<String>() {
        @Override
        public void success(String s, Response response) {

        }

        @Override
        public void failure(RetrofitError error) {

        }
    });


    }


   public User getUserDetailsFromFB(){

       final User userDetails = new User();

       GraphRequest newrequest = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
               new GraphRequest.GraphJSONObjectCallback() {
                   @Override
                   public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {

                       Profile profile = Profile.getCurrentProfile();

                       try {

                           userDetails.setUserid(AccessToken.getCurrentAccessToken().getUserId());
                           userDetails.setGender(jsonObject.getString("gender"));
                           userDetails.setBirthday(jsonObject.getString("birthday"));
                           userDetails.setLocation(jsonObject.getJSONObject("location").getString("name"));
                           userDetails.setProfileid(profile.getId());
                           userDetails.setName(profile.getName());




                       } catch (JSONException e) {
                           e.printStackTrace();
                       }
                   }
               });
       Bundle parameters = new Bundle();
       parameters.putString("fields", "id,name,link,birthday,email,location,gender,picture");
       newrequest.setParameters(parameters);
       newrequest.executeAndWait();


       return userDetails;
   }

    public void saveSessionToServer(Session session) {

        restClient.addSession(session, new Callback<String>() {
            @Override
            public void success(String s, Response response) {

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }

    public void savePostToServer(final Post post, String[] imagesPath){
        final MultipartTypedOutput files = new MultipartTypedOutput();
        int imagesArrayLength = Utils.getCurrentLength(imagesPath);
        for(int i=0;i<imagesArrayLength;i++){
            files.addPart("files",new TypedFile("application/octet-stream",new File(imagesPath[i])));
        }


        restClient.addPost(post, new Callback<String>() {
            @Override
            public void success(String postId, Response response) {

                // ((MainApplication) ApplicationContextHolder.getApplicationContext()).getMapAppService().renderMarkersOnMap();

                if (files.getPartCount() > 0) {

                    restClient.addUserPostImage(post.getUserid(), postId, files);


                }


            }

            @Override
            public void failure(RetrofitError error) {
                System.out.println();
            }
        });

    }

    public List<FeedItem> getUserFeedItems(){


        return null;
    }


    public void fetchAndSaveUserPostById(String postId) {

         UserPost userPost = restClient.getUserPostById(postId);

                ImageManager.downloadIfNotExist(userPost);


                NearbyPost nearbyPost = new NearbyPost();
                nearbyPost.setUserid(userPost.getUser().getUserid());
                nearbyPost.setPostid(userPost.getPost().getPostid());
                nearbyPost.setPost(userPost.getPost().getPost());
                nearbyPost.setLatitude(userPost.getPost().getLatitude());
                nearbyPost.setLongitude(userPost.getPost().getLongitude());
                nearbyPost.setTimestamp(userPost.getPost().getTimestamp());
                nearbyPost.setTotalcomments(userPost.getTotalcomments());
                nearbyPost.setTotallikes(userPost.getTotallikes());
                nearbyPost.setTotaldislikes(userPost.getTotaldisLikes());


                try {
                    saveToLocalDB(userPost.getUser(), DaoEnum.USER, "userid", userPost.getUser().getUserid());
                    saveToLocalDB(nearbyPost, DaoEnum.NEARBYPOST, "postid", String.valueOf(userPost.getPost().getPostid()));

                    NearbyPostImage nearbyPostImage = new NearbyPostImage();
                    nearbyPostImage.setPostid(userPost.getPost().getPostid());

                    for (String fileLocation : userPost.getThumbnaillocation()) {

                        nearbyPostImage.setPostid(userPost.getPost().getPostid());
                        nearbyPostImage.setThumbnailLocation(fileLocation);
                    }
                    for (String fileLocation : userPost.getActuallocation()) {

                        nearbyPostImage.setPostid(userPost.getPost().getPostid());
                        nearbyPostImage.setActuallocation(fileLocation);

                    }

                    saveToLocalDB(nearbyPostImage, DaoEnum.NEARBYPOSTIMAGE, "postid", String.valueOf(userPost.getPost().getPostid()));
                    new GetCommentsData(userPost.getPost().getPostid(),restClient).execute();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

    }

    public void savePostComment(NearbyPostComment postComment,Callback cb) {
       restClient.savePostComment(postComment,cb);
    }

    public void saveActionToServer(UserPostLike userPostLike) {

        restClient.savePostAction(userPostLike, new Callback<Boolean>() {
            @Override
            public void success(Boolean aBoolean, Response response) {

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }
}

