package com.wannashare.datahelper.websockets;

import android.content.Context;
import android.content.Intent;
import android.util.Log;


import com.facebook.AccessToken;
import com.wannashare.activities.MainActivity;
import com.wannashare.application.MainApplication;
import com.wannashare.datahelper.async.post.GetCommentsData;
import com.wannashare.datahelper.async.post.GetPostActionData;
import com.wannashare.datahelper.async.post.GetUserPost;
import com.wannashare.datahelper.user.UserDataHelper;
import com.wannashare.utils.Constants;
import com.wannashare.wrappers.ContextStore;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.handshake.ServerHandshake;


import java.net.URI;
import java.sql.SQLException;


public class WebSocketDataHelper extends WebSocketClient {


    Context serviceContext;
    UserDataHelper userDataHelper = new UserDataHelper();
    Intent commentIntent = new Intent();
    Intent postIntent = new Intent();




    /**
     * Constructs a WebSocketClient instance and sets it to the connect to the
     * specified URI. The channel does not attampt to connect automatically. You
     * must call <var>connect</var> first to initiate the socket connection.
     *
     * @param serverUri
     * @param draft
     */
    public WebSocketDataHelper(URI serverUri, Draft draft,Context context) throws SQLException {
        super(serverUri, draft);
        this.serviceContext = context;
        commentIntent.setAction(Constants.COMMENT_BROADCAST_ACTION);
        postIntent.setAction(Constants.POST_BROADCAST_ACTION);

    }


    @Override
    public void onOpen(ServerHandshake handshakedata) {
        Log.i("Websocket", "Opened");
        send(AccessToken.getCurrentAccessToken().getUserId());
    }

    @Override
    public void onMessage(String message) {
        if(!message.contains("handshake")){
           // new WSDataAsyncTask(message,postIntent,commentIntent,serviceContext).execute();
            String header = message.split("\\:")[0];
            if(header.contains("Post")) {
                String postId = message.split("\\:")[1];
                userDataHelper.fetchAndSaveUserPostById(postId);
            }
            else if(header.contains("Comment")){
                String postId = message.split("\\:")[1];
                try {
                  //  new GetUserPost(Integer.parseInt(postId),userDataHelper.getRestClient()).execute();
                    new GetCommentsData(Integer.parseInt(postId),userDataHelper.getRestClient(),serviceContext,commentIntent).execute();

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            else if(header.contains("Action")){
                String postId = message.split("\\:")[1];

                    //  new GetUserPost(Integer.parseInt(postId),userDataHelper.getRestClient()).execute();
                    new GetPostActionData(Integer.parseInt(postId),userDataHelper.getRestClient()).execute();


            }
            System.out.println();



        }

    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        Log.i("Websocket", "Closed " + reason);
    }

    @Override
    public void onError(Exception ex) {
        Log.i("Websocket", "Error " + ex.getMessage());
    }
}
