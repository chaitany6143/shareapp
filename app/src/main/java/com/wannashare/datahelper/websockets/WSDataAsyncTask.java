package com.wannashare.datahelper.websockets;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.wannashare.datahelper.async.post.GetCommentsData;
import com.wannashare.datahelper.user.UserDataHelper;
import com.wannashare.localdatastore.models.NearbyPost;
import com.wannashare.utils.Constants;
import com.wannashare.utils.DaoEnum;
import com.wannashare.wrappers.ContextStore;

import java.math.BigInteger;
import java.sql.SQLException;

/**
 * Created by chaitanya on 9/28/15.
 */
public class WSDataAsyncTask extends AsyncTask<Object, Object, Object> {
    String message;
    UserDataHelper userDataHelper;
    Intent postIntent;
    Intent commentIntent;
    Context serviceContext;
    public WSDataAsyncTask(String message, Intent postIntent, Intent commentIntent, Context serviceContext){
      this.message = message;
        this.postIntent = postIntent;
        this.commentIntent = commentIntent;
        this.serviceContext = serviceContext;
        try {
            userDataHelper = new UserDataHelper();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p/>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected Object doInBackground(Object... params) {
        String header = message.split("\\:")[0];
        if(header.contains("Post")) {
            String postId = message.split("\\:")[1];
            userDataHelper.fetchAndSaveUserPostById(postId);
        }
        else if(header.contains("Comment")){
            String postId = message.split("\\:")[1];
            try {
                new GetCommentsData(Integer.parseInt(postId),userDataHelper.getRestClient()).execute();

                serviceContext.sendBroadcast(commentIntent);
                //NearbyPost post = (NearbyPost) userDataHelper.getFromLocalDbByField("postid", postId, DaoEnum.NEARBYPOST);
                // BigInteger totalComments = post.getTotalcomments().add(new BigInteger(String.valueOf(1)));
                // userDataHelper.updateInLocalDB("totalcomments",totalComments.toString(),DaoEnum.NEARBYPOST,"postid",postId);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
return null;
    }
}
