package com.wannashare.datahelper.maps;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.wannashare.adapters.PostListAdapter;
import com.wannashare.localdatastore.models.UserPost;
import com.wannashare.datahelper.DataHelper;
import com.wannashare.datahelper.user.UserDataHelper;
import com.wannashare.utils.Constants;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by chaitanya on 6/24/15.
 */
public class MapDataHelperDelete extends DataHelper implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener{

    /**
     * *************** Declarations ********************************
     */

    Context context;

    //Location based variables
    private LocationRequest locationRequest;
    private GoogleApiClient locationClient;
    private Location lastLocation;
    private Location currentLocation;

    // Stores the current instantiation of the location client in this object

    private Circle mapCircle;
    private float radius;
    private float lastRadius;

    PostListAdapter postListAdapter;
    GoogleMap googleMap;
    LocationManager locationManager;
    private final Map<String, Marker> postIdMarkerMap = new HashMap<String, Marker>();
    private boolean hasSetUpInitialLocation;
    UserDataHelper userService;
    private int mostRecentMapUpdate;
    private String selectedPostObjectId="";

    private final Map<String, Marker> mapMarkers = new HashMap<String, Marker>();


    /**
     * ***************Getters and Setters********************************
     */

    public Location getLastLocation() {
        return lastLocation;
    }

    public void setLastLocation(Location lastLocation) {
        this.lastLocation = lastLocation;
    }



    public void setCurrentLocation(Location currentLocation) {
        this.currentLocation = currentLocation;
    }

    /**
     * ***************Constructor********************************
     *
     *
     */
    public MapDataHelperDelete(Context appContext) throws SQLException {

        super();
        locationRequest = LocationRequest.create();

        // Set the update interval
        locationRequest.setInterval(Constants.UPDATE_INTERVAL_IN_MILLISECONDS);

        // Use high accuracy
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Set the interval ceiling to one minute
        locationRequest.setFastestInterval(Constants.FAST_CEILING_IN_MILLISECONDS);

        // Create a new location client, using the enclosing class to handle callbacks.
        locationClient = new GoogleApiClient.Builder(appContext)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    public void initialize(Context context,GoogleMap googleMap) {

        this.googleMap=googleMap;
        this.context=context;
        radius =Float.valueOf(getFromLocalCache(Constants.SEARCH_DISTANCE));
        lastRadius = radius;



    }

    /**
       Connection callbacks
     */
    @Override
    public void onConnected(Bundle bundle) {

        Log.d("Connected to location services", Constants.APP_TAG);

        currentLocation = getLocation();
        startPeriodicUpdates();
    }
    @Override
    public void onConnectionSuspended(int i) {
        Log.i(Constants.APP_TAG, "GoogleApiClient connection has been suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {

            //ToDo :  Start an Activity that tries to resolve the error
            // connectionResult.startResolutionForResult(this, Constants.CONNECTION_FAILURE_RESOLUTION_REQUEST);

        }
        else {
            // If no resolution is available, display a dialog to the user with the error.
            Toast.makeText(context, connectionResult.getErrorCode(), Toast.LENGTH_LONG).show();

        }
    }

    /**
     * Connection util methods
     */
    public void connectToLocationService(){
        locationClient.connect();
    }


    public void disconnectFromLocationService(){
        if(locationClient.isConnected())
            locationClient.disconnect();
    }


    public boolean isLocationServicesConnected() {
        // Check that Google Play services is available
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);

        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode){
            Log.d(Constants.APP_TAG, "Google play services available");
            return true;
        }
        else{

            Toast.makeText(context, "Could not connect to the location services!", Toast.LENGTH_LONG).show();
            return false;
        }
    }


    /**
     * Periodic updates functionality
     */
    private void startPeriodicUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                locationClient, locationRequest, this);
    }

    private void stopPeriodicUpdates() {
        locationClient.disconnect();
    }

    /**
     * Get the current location
     * @return
     */

    private Location getLocation() {
        // If Google Play Services is available
        if (isLocationServicesConnected()) {
            // Get the current location
            return LocationServices.FusedLocationApi.getLastLocation(locationClient);
        } else {
            return null;
        }
    }


    /**
     * On location change handler
     * @param location
     */


    @Override
    public void onLocationChanged(Location location) {
        currentLocation=location;
        if(lastLocation!=null && currentLocation.distanceTo(lastLocation)<0.01){
            return;
        }
        lastLocation = location;
        LatLng latLngObj = new LatLng(location.getLatitude(), location.getLongitude());

        if (!hasSetUpInitialLocation) {
            // Zoom to the current location.
            updateZoom(latLngObj);
            hasSetUpInitialLocation = true;
        }

            updateCircle(latLngObj);

            renderMarkersOnMap();


    }
    /***
     * Map zoom handler
     */
    public void updateZoom(LatLng myLatLng) {
        // Get the bounds to zoom to
        LatLngBounds bounds = calculateBoundsWithCenter(myLatLng);
        // Zoom to the given bounds
        googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 5));
    }
    /*
    * Helper method to calculate the bounds for map zooming
    */
    LatLngBounds calculateBoundsWithCenter(LatLng myLatLng) {
        // Create a bounds
        LatLngBounds.Builder builder = LatLngBounds.builder();

        // Calculate east/west points that should to be included
        // in the bounds
        double lngDifference = calculateLatLngOffset(myLatLng, false);
        LatLng east = new LatLng(myLatLng.latitude, myLatLng.longitude + lngDifference);
        builder.include(east);
        LatLng west = new LatLng(myLatLng.latitude, myLatLng.longitude - lngDifference);
        builder.include(west);

        // Calculate north/south points that should to be included
        // in the bounds
        double latDifference = calculateLatLngOffset(myLatLng, true);
        LatLng north = new LatLng(myLatLng.latitude + latDifference, myLatLng.longitude);
        builder.include(north);
        LatLng south = new LatLng(myLatLng.latitude - latDifference, myLatLng.longitude);
        builder.include(south);

        return builder.build();
    }

    private double calculateLatLngOffset(LatLng myLatLng, boolean bLatOffset) {
        // The return offset, initialized to the default difference
        double latLngOffset = Constants.OFFSET_CALCULATION_INIT_DIFF;
        // Set up the desired offset distance in meters
        float desiredOffsetInMeters = radius * Constants.METERS_PER_FEET;
        // Variables for the distance calculation
        float[] distance = new float[1];
        boolean foundMax = false;
        double foundMinDiff = 0;
        // Loop through and get the offset
        do {
            // Calculate the distance between the point of interest
            // and the current offset in the latitude or longitude direction
            if (bLatOffset) {
                Location.distanceBetween(myLatLng.latitude, myLatLng.longitude, myLatLng.latitude
                        + latLngOffset, myLatLng.longitude, distance);
            } else {
                Location.distanceBetween(myLatLng.latitude, myLatLng.longitude, myLatLng.latitude,
                        myLatLng.longitude + latLngOffset, distance);
            }
            // Compare the current difference with the desired one
            float distanceDiff = distance[0] - desiredOffsetInMeters;
            if (distanceDiff < 0) {
                // Need to catch up to the desired distance
                if (!foundMax) {
                    foundMinDiff = latLngOffset;
                    // Increase the calculated offset
                    latLngOffset *= 2;
                } else {
                    double tmp = latLngOffset;
                    // Increase the calculated offset, at a slower pace
                    latLngOffset += (latLngOffset - foundMinDiff) / 2;
                    foundMinDiff = tmp;
                }
            } else {
                // Overshot the desired distance
                // Decrease the calculated offset
                latLngOffset -= (latLngOffset - foundMinDiff) / 2;
                foundMax = true;
            }
        } while (Math.abs(distance[0] - desiredOffsetInMeters) > Constants.OFFSET_CALCULATION_ACCURACY);
        return latLngOffset;
    }

    public void updateCircle(LatLng myLatLng) {
        if (mapCircle == null) {
            mapCircle =
                    googleMap.addCircle(
                            new CircleOptions().center(myLatLng).radius(radius * Constants.METERS_PER_FEET));
            int baseColor = Color.DKGRAY;
            mapCircle.setStrokeColor(baseColor);
            mapCircle.setStrokeWidth(2);
            mapCircle.setFillColor(Color.argb(50, Color.red(baseColor), Color.green(baseColor),
                    Color.blue(baseColor)));
        }
        mapCircle.setCenter(myLatLng);
        mapCircle.setRadius(radius * Constants.METERS_PER_FEET); // Convert radius in feet to meters.
    }

    private void removeMarkersFromMap(Set<String> markersToKeep) {
        for (String objId : new HashSet<String>(mapMarkers.keySet())) {
            if (!markersToKeep.contains(objId)) {
                Marker marker = mapMarkers.get(objId);
                marker.remove();
                mapMarkers.get(objId).remove();
                mapMarkers.remove(objId);
            }
        }
    }

    public void renderMarkersOnMap() {

        final int myUpdateNumber = ++mostRecentMapUpdate;
        final Location userLocation = (currentLocation == null) ? lastLocation : currentLocation;
        // If location info isn't available, clean up any existing markers
        if (userLocation == null) {
            removeMarkersFromMap(new HashSet<String>());
            return;
        }

        restClient.getNearbyPosts(new Callback<List<UserPost>>() {
            @Override
            public void success(List<UserPost> userPosts, Response response) {

                localCacheAccessor.persistToCache("userFeed",userPosts.toString());

                if (myUpdateNumber != mostRecentMapUpdate) {
                    return;
                }
                // Posts to show on the map
                Set<String> toKeep = new HashSet<String>();

                for (UserPost userPost : userPosts) {

                    // Add this post to the list of map pins to keep
                    toKeep.add(String.valueOf(userPost.getPost().getPostid()));
                    // Check for an existing marker for this post
                    Marker oldMarker = mapMarkers.get(userPost.getPost().getPostid());

                    // Set up the map marker's location
                    MarkerOptions markerOpts =
                            new MarkerOptions().position(new LatLng(userPost.getPost().getLatitude(), userPost.getPost().getLongitude()));

                    Location postLocation = new Location("");
                    postLocation.setLatitude(userPost.getPost().getLatitude());
                    postLocation.setLongitude(userPost.getPost().getLongitude());
                    if (postLocation.distanceTo(userLocation) > (radius * Constants.METERS_PER_FEET)) {

                        if (oldMarker != null) {
                            if (oldMarker.getSnippet() == null) {
                                // Out of range marker already exists, skip adding it
                                continue;
                            } else {
                                // Marker now out of range, needs to be refreshed
                                oldMarker.remove();
                            }
                        }
                        // Display a red marker with a predefined title and no snippet
                        markerOpts =
                                markerOpts.title("Can\\'t view post! Get closer.").icon(
                                        BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                    } else {
                        // Check for an existing in range marker
                        if (oldMarker != null) {
                            if (oldMarker.getSnippet() != null) {
                                // In range marker already exists, skip adding it
                                continue;
                            } else {
                                // Marker now in range, needs to be refreshed
                                oldMarker.remove();
                            }
                        }
                        // Display a green marker with the post information
                        markerOpts =
                                markerOpts.title(userPost.getPost().getPost()).snippet(userPost.getUser().getName())
                                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                    }
                    // Add a new marker
                    Marker marker = googleMap.addMarker(markerOpts);
                    mapMarkers.put(String.valueOf(userPost.getPost().getPostid()), marker);

                }
                // Clean up old markers.
                removeMarkersFromMap(toKeep);
            }


            @Override
            public void failure(RetrofitError error) {
                Log.d(Constants.APP_TAG, "An error occurred while querying for map posts.",error);
            }
        });



    }










}
