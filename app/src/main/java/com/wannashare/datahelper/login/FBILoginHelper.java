package com.wannashare.datahelper.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.wannashare.R;
import com.wannashare.activities.MainActivity;

import com.wannashare.datahelper.async.login.SaveLoginData;
import com.wannashare.localdatastore.models.Session;
import com.wannashare.localdatastore.models.User;
import com.wannashare.datahelper.user.UserDataHelper;
import com.wannashare.utils.ActivityEnum;
import com.wannashare.wrappers.ContextStore;


import java.sql.SQLException;
import java.util.Arrays;


/**
 * Created by chaitanya on 5/19/15.
 */
public class FBILoginHelper implements ILoginHelper {

    ProgressDialog progressDialog;
    CallbackManager callbackManager = CallbackManager.Factory.create();
    Context context;

    public FBILoginHelper(ActivityEnum activityEnum){
       context = ContextStore.getInstance().getContext(activityEnum);
    }
    @Override
    public void handleLogin() {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.activity_login, null);

        LoginButton fbLoginButton = (LoginButton)v.findViewById(R.id.fb_login_button);


        fbLoginButton.setReadPermissions(Arrays.asList("public_profile", "email", "user_location", "user_birthday"));

        // NOTE: for extended permissions, like "user_about_me", your app must be reviewed by the Facebook team
        // (https://developers.facebook.com/docs/facebook-login/permissions/)


        fbLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            private ProfileTracker mProfileTracker;

            @Override
            public void onSuccess(LoginResult loginResult) {
             /*   mProfileTracker = new ProfileTracker() {
                    @Override
                    protected void onCurrentProfileChanged(Profile profile, Profile profile2) {
                        Log.v("facebook - profile", profile2.getFirstName());
                        mProfileTracker.stopTracking();
                    }
                };
                mProfileTracker.startTracking(); */
                final LoginResult currentLoginResult = loginResult;
                System.out.println("onSuccess");
                UserDataHelper userDataHelper = null;
                try {
                    userDataHelper = new UserDataHelper();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                AccessToken accessToken = loginResult.getAccessToken();

                Session session = new Session();

                session.setToken(accessToken.getToken());
                session.setUserid(accessToken.getUserId());

                new SaveLoginData(new User(),session).execute();

                context.startActivity(new Intent(context, MainActivity.class));

            }

            @Override
            public void onCancel() {
                // App code
                System.out.println("onCancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                System.out.println("onError");
            }
        });

    }


    @Override
    public void handleOnActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode,resultCode,data);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {


    }
}
