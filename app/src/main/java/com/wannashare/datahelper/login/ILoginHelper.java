package com.wannashare.datahelper.login;

import android.content.Intent;
import android.view.View;

/**
 * Created by chaitanya on 5/19/15.
 */
public interface ILoginHelper extends View.OnClickListener {

    @Override
    void onClick(View v);

    void handleLogin();

    void handleOnActivityResult(int requestCode, int resultCode, Intent data);
}
