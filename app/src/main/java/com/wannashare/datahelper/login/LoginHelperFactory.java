package com.wannashare.datahelper.login;

import com.wannashare.utils.ActivityEnum;


/**
 * Created by chaitanya on 5/19/15.
 */
public class LoginHelperFactory {

    ILoginHelper handler;


    public static ILoginHelper getHandler(String loginType,ActivityEnum activityEnum){


         if(loginType.equalsIgnoreCase("facebook"))
            return new FBILoginHelper(activityEnum);
        else
            return null;

    }
}
