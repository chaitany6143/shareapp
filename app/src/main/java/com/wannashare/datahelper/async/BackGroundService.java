package com.wannashare.datahelper.async;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.wannashare.application.MainApplication;
import com.wannashare.datahelper.async.locationdata.FetchLocationDataThread;
import com.wannashare.datahelper.async.locationdata.LocationUtils;
import com.wannashare.datahelper.location.LocationDataHelper;
import com.wannashare.datahelper.websockets.WebSocketDataHelper;
import com.wannashare.utils.Constants;
import com.wannashare.wrappers.ContextStore;
import com.wannashare.localdatastore.LocalCacheAccessor;

import org.java_websocket.drafts.Draft_17;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;

public class BackGroundService extends Service {


    public LocationDataHelper locationDataHelper;
    public LocationManager locationManager;
    public MyLocationListener listener;
    public Location previousBestLocation = null;
    public LocalCacheAccessor localCacheAccessor;
    public WebSocketDataHelper webSocketDataHelper;



    Intent intent;
    int counter = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        intent = new Intent();
        intent.setAction(Constants.LOCATION_BROADCAST_ACTION);

        localCacheAccessor = ((MainApplication)ContextStore.getApplicationContext()).getLocalCacheAccessor();



    }

    @Override
    public void onStart(Intent intent, int startId) {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        listener = new MyLocationListener();
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 4000, 0, listener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 4000, 0, listener);
        try {
           // webSocketDataHelper = new WebSocketDataHelper(new URI("ws://10.0.0.58:8080/wannashare/websocket"),new Draft_17(),this);
            webSocketDataHelper = new WebSocketDataHelper(new URI("ws://52.11.67.165:8080/wannashare/websocket"),new Draft_17(),this);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        webSocketDataHelper.connect();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }




    @Override
    public void onDestroy() {
        // handler.removeCallbacks(sendUpdatesToUI);
        super.onDestroy();
        Log.v("STOP_SERVICE", "DONE");
        locationManager.removeUpdates(listener);
        try {
            webSocketDataHelper.closeBlocking();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



    public class MyLocationListener implements LocationListener

    {
        Thread backGroundThread;
        public void onLocationChanged(final Location loc)
        {
            Log.i("**************************************", "Location changed");

           // if(LocationUtils.isBetterLocation(loc, previousBestLocation)) {
                localCacheAccessor.persistToCache("currentLocation",String.valueOf(loc.
                        getLatitude())+","+String.valueOf(loc.getLongitude()));
                try {
                    backGroundThread = new FetchLocationDataThread(loc);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                backGroundThread.start();
                loc.getLatitude();
                loc.getLongitude();
                intent.putExtra("Latitude", loc.getLatitude());
                intent.putExtra("Longitude", loc.getLongitude());
                sendBroadcast(intent);


          //  }
        }

        public void onProviderDisabled(String provider)
        {
            Toast.makeText(getApplicationContext(), "Gps Disabled", Toast.LENGTH_SHORT).show();
        }


        public void onProviderEnabled(String provider)
        {
            Toast.makeText( getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();
        }


        public void onStatusChanged(String provider, int status, Bundle extras)
        {

        }

    }

}