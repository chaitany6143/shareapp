package com.wannashare.datahelper.async.post;

import android.os.AsyncTask;

import com.wannashare.datahelper.user.UserDataHelper;
import com.wannashare.localdatastore.models.UserPost;
import com.wannashare.localdatastore.models.UserPostLike;
import com.wannashare.utils.DaoEnum;
import com.wannashare.utils.IRestApiClient;

import java.sql.SQLException;

/**
 * Created by chaitanya on 10/8/15.
 */
public class GetPostActionData extends AsyncTask<Object, Object, Object> {


    UserDataHelper userDataHelper;
    int postid;
    IRestApiClient restApiClient;


    public GetPostActionData(int postid, IRestApiClient restClient) {
this.restApiClient = restClient;
        this.postid = postid;
    }

    @Override
    protected Object doInBackground(Object... params) {
        UserPost userPost = restApiClient.getUserPostById(String.valueOf(postid));
        try {

            if (userDataHelper.isExistInDB(userPost, DaoEnum.NEARBYPOST, "postid", String.valueOf(userPost.getPost().getPostid()))) {

                userDataHelper.updateInLocalDB("totallikes", userPost.getTotallikes(),DaoEnum.NEARBYPOST,"postid", String.valueOf(userPost.getPost().getPostid()));
                userDataHelper.updateInLocalDB("totaldislikes", userPost.getTotaldisLikes(),DaoEnum.NEARBYPOST,"postid", String.valueOf(userPost.getPost().getPostid()));
                userDataHelper.updateInLocalDB( "totalcomments", userPost.getTotalcomments(), DaoEnum.NEARBYPOST,"postid", String.valueOf(userPost.getPost().getPostid()));

            }
        } catch (SQLException e1) {
            e1.printStackTrace();
        }



        return null;
    }

    /**
     * <p>Runs on the UI thread after {@link #doInBackground}. The
     * specified result is the value returned by {@link #doInBackground}.</p>
     * <p/>
     * <p>This method won't be invoked if the task was cancelled.</p>
     *
     * @param o The result of the operation computed by {@link #doInBackground}.
     * @see #onPreExecute
     * @see #doInBackground
     * @see #onCancelled(Object)
     */
    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);




    }
}
