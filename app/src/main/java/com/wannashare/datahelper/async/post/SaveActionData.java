package com.wannashare.datahelper.async.post;

import android.content.Context;
import android.os.AsyncTask;

import com.wannashare.adapters.FeedImagesAdapter;
import com.wannashare.adapters.FeedListAdapter;
import com.wannashare.datahelper.user.UserDataHelper;
import com.wannashare.localdatastore.models.NearbyPost;
import com.wannashare.localdatastore.models.NearbyPostImage;
import com.wannashare.localdatastore.models.User;
import com.wannashare.localdatastore.models.UserPostLike;
import com.wannashare.utils.DaoEnum;

import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by chaitanya on 10/8/15.
 */
public class SaveActionData extends AsyncTask<Object, Object, Object> {


    UserDataHelper userDataHelper;
    UserPostLike userPostLike;


    public SaveActionData(UserPostLike userPostLike) {
        this.userPostLike = userPostLike;
    }

    @Override
    protected Object doInBackground(Object... params) {

        try {
            UserDataHelper userDataHelper = new UserDataHelper();
           userDataHelper.saveActionToServer(userPostLike);
            userDataHelper.saveToLocalCache(String.valueOf(userPostLike.getPostid()),String.valueOf(userPostLike.getAction()));
        } catch (SQLException e) {
            e.printStackTrace();
        }




        return null;
    }

    /**
     * <p>Runs on the UI thread after {@link #doInBackground}. The
     * specified result is the value returned by {@link #doInBackground}.</p>
     * <p/>
     * <p>This method won't be invoked if the task was cancelled.</p>
     *
     * @param o The result of the operation computed by {@link #doInBackground}.
     * @see #onPreExecute
     * @see #doInBackground
     * @see #onCancelled(Object)
     */
    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);

        


    }
}
