package com.wannashare.datahelper.async.locationdata;

import android.location.Location;

import com.wannashare.datahelper.location.LocationDataHelper;

import java.sql.SQLException;

/**
 * Created by chaitanya on 9/19/15.
 */
public class FetchLocationDataThread extends Thread {
    LocationDataHelper locationDataHelper;
    Location location;


    public FetchLocationDataThread(Location location) throws SQLException {
        this.locationDataHelper = new LocationDataHelper();
        this.location = location;

    }

    /**
     * Calls the <code>run()</code> method of the Runnable object the receiver
     * holds. If no Runnable is set, does nothing.
     *
     * @see Thread#start
     */
    @Override
    public void run() {
        locationDataHelper.fetchAndSaveLocationData(location);
    }
}
