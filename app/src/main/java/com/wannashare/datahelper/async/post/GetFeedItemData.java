package com.wannashare.datahelper.async.post;

import android.content.Context;
import android.os.AsyncTask;

import com.wannashare.adapters.FeedImagesAdapter;
import com.wannashare.adapters.FeedListAdapter;
import com.wannashare.application.MainApplication;
import com.wannashare.datahelper.user.UserDataHelper;
import com.wannashare.localdatastore.models.NearbyPost;
import com.wannashare.localdatastore.models.NearbyPostComment;
import com.wannashare.localdatastore.models.NearbyPostImage;
import com.wannashare.localdatastore.models.User;
import com.wannashare.utils.DaoEnum;
import com.wannashare.wrappers.ContextStore;
import com.wannashare.wrappers.LruBitmapCache;

import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by chaitanya on 10/6/15.
 */
public class GetFeedItemData extends AsyncTask<Object, Object, Object> {

    private final FeedListAdapter.ViewHolder mHolder;
    UserDataHelper userDataHelper;
    User userInfo = null;
    List<NearbyPostImage> nearbyPostImages = null;
    NearbyPost nearbyPost;
    Context activity;



public GetFeedItemData(FeedListAdapter.ViewHolder viewHolder,NearbyPost nearbyPost,Context context) throws SQLException {

        userDataHelper = new UserDataHelper();
    this.mHolder = viewHolder;
    this.nearbyPost = nearbyPost;
    this.activity = context;
        }

@Override
protected Object doInBackground(Object... params) {

    try {
        UserDataHelper userDataHelper = new UserDataHelper();
        userInfo = (User) (userDataHelper.getFromLocalDbByField("userid", nearbyPost.getUserid(), DaoEnum.USER)).get(0);
        nearbyPostImages = (List<NearbyPostImage>) userDataHelper.getFromLocalDbByField("postid", String.valueOf(nearbyPost.getPostid()), DaoEnum.NEARBYPOSTIMAGE);

    } catch (SQLException e) {
        e.printStackTrace();
    }




        return null;
        }

    /**
     * <p>Runs on the UI thread after {@link #doInBackground}. The
     * specified result is the value returned by {@link #doInBackground}.</p>
     * <p/>
     * <p>This method won't be invoked if the task was cancelled.</p>
     *
     * @param o The result of the operation computed by {@link #doInBackground}.
     * @see #onPreExecute
     * @see #doInBackground
     * @see #onCancelled(Object)
     */
    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);

        mHolder.name.setText(userInfo.getName());
        Date date = new Date(Long.parseLong(nearbyPost.getTimestamp()));

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy','HH:mm:ss");

        String dateString = simpleDateFormat.format(date);
        mHolder.timestamp.setText(dateString);
        mHolder.statusMsg.setText(nearbyPost.getPost());

        mHolder.profilePictureView.setProfileId(userInfo.getProfileid());

        mHolder.gallery.setAdapter(new FeedImagesAdapter(activity, nearbyPostImages,nearbyPost.getUserid()));


    }
}
