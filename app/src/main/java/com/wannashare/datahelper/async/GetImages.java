package com.wannashare.datahelper.async;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;


import com.wannashare.application.MainApplication;
import com.wannashare.localdatastore.models.UserPost;
import com.wannashare.utils.IRestApiClient;
import com.wannashare.utils.ImageManager;
import com.wannashare.wrappers.ContextStore;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


import retrofit.RestAdapter;


public class GetImages extends AsyncTask<Object, Object, Object> {
    private String requestUrl, imagename_;
    private ImageView view;
    private Bitmap bitmap ;
    private FileOutputStream fos;
    private UserPost userPost;
    public GetImages(String requestUrl,UserPost userPost) {
        this.requestUrl = requestUrl;
        this.userPost=userPost;

    }

    @Override
    protected Object doInBackground(Object... objects) {

       // String imageUrl = "http://10.0.0.18:8080/wannashare/user/images?fileLocation=" + requestUrl;

        RestAdapter restAdapter = ((MainApplication) ContextStore.getApplicationContext()).getRestAdapter();

        IRestApiClient restClient = restAdapter.create(IRestApiClient.class);

        retrofit.client.Response response = null;

        response = restClient.getImages(requestUrl);

        try {
            byte[] bytes = getBytesFromStream(response.getBody().in());
            bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        } catch (IOException e) {
            e.printStackTrace();
        }



        //
     //   downloadImage(imageUrl);

       /* URLConnection connection;

        URL url = null;
        try {
            url = new URL("http://10.0.0.18:8080/wannashare/images?fileLocation=" + requestUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        byte[] imageByteArray = new byte[0];
        try {
            imageByteArray = getImage(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        bitmap = BitmapFactory.decodeByteArray(imageByteArray, 0, imageByteArray.length);  */

        return null;
    }



    /*@TargetApi(Build.VERSION_CODES.KITKAT)
    public static byte[] getImage(URL url) throws IOException {
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestProperty("accept", "image/jpeg");

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header


        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString().getBytes(Charset.forName("UTF-8"));
    }

*/
    @Override
    protected void onPostExecute(Object o) {

        String relativeLocation = userPost.getUser().getUserid()+"/"+userPost.getPost().getPostid()+"/thumbnail";
            ImageManager.saveToSdCard(bitmap, relativeLocation, requestUrl.substring(requestUrl.lastIndexOf("/") + 1));

    }
    /*private String downloadImage(String imageUrl) {
        ClientConfig clientConfig = null;
        Client client = null;
        WebTarget webTarget = null;
        Invocation.Builder invocationBuilder = null;
        Response response = null;
        InputStream inputStream = null;
        OutputStream outputStream = null;
        int responseCode;
        String responseMessageFromServer = null;
        String responseString = null;
        String qualifiedDownloadFilePath = null;

        try{
            // invoke service after setting necessary parameters
            clientConfig = new ClientConfig();
            clientConfig.register(MultiPartFeature.class);
            client =  ClientBuilder.newClient(clientConfig);
            client.property("accept", "image/png");
            webTarget = client.target(httpURL);

            // invoke service
            invocationBuilder = webTarget.request();
            //          invocationBuilder.header("Authorization", "Basic " + authorization);
            response = invocationBuilder.get();

            // get response code
            responseCode = response.getStatus();
            System.out.println("Response code: " + responseCode);

            if (response.getStatus() != 200) {
                throw new RuntimeException("Failed with HTTP error code : " + responseCode);
            }

            // get response message
            responseMessageFromServer = response.getStatusInfo().getReasonPhrase();
            System.out.println("ResponseMessageFromServer: " + responseMessageFromServer);

            // read response string
            inputStream = response.readEntity(InputStream.class);
            qualifiedDownloadFilePath = DOWNLOAD_FILE_LOCATION + "MyJerseyImage.png";
            outputStream = new FileOutputStream(qualifiedDownloadFilePath);
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            // set download SUCCES message to return
            responseString = "downloaded successfully at " + qualifiedDownloadFilePath;
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        finally{
            // release resources, if any
            outputStream.close();
            response.close();
            client.close();
        }
        return responseString;
    }*/
   // }

   /* public String testDownloadService(String httpURL) throws IOException {

        // local variables
        ClientRequest clientRequest = null;
        ClientResponse<?> clientResponse = null;
        File readSourceFile = null;
        File destinationFileLocation = null;
        FileWriter fileWriter = null;
        int responseCode;
        String responseMessageFromServer = null;
        String responseString = null;
        String qualifiedDownloadFilePath = null;

        try {
            clientRequest = new ClientRequest(httpURL);
            clientRequest.setHttpMethod(HttpMethod.GET);
            clientRequest.header("accept", "image/png");
            clientResponse = clientRequest.get();

            // get response code
            responseCode = clientResponse.getStatus();
            System.out.println("Response code: " + responseCode);

            if (clientResponse.getStatus() != 200) {
                throw new RuntimeException("Failed with HTTP error code : " + responseCode);
            }

            // get response message
            responseMessageFromServer = clientResponse.getResponseStatus().getReasonPhrase();
            System.out.println("ResponseMessageFromServer: " + responseMessageFromServer);

            // read response string
            readSourceFile = (File) clientResponse.getEntity(File.class);
            // yaha !!

            File sdcard = Environment.getExternalStorageDirectory();

            FileUtil.createAppDirIfNotExist();

            String relativeLocation = userPost.getUser().getUserid() + "/" + userPost.getPost().getPostid();
            File destinationDir = new File(FileUtil.getAppFolder() + "/" + relativeLocation);
            if (!destinationDir.exists()) {
                boolean created = destinationDir.mkdirs();
                if (!created) {
                    try {
                        throw new IOException("Unable to create destination dir " + destinationDir);
                    } catch (IOException e) {

                    }
                }

                qualifiedDownloadFilePath = destinationDir + "/" + requestUrl.substring(requestUrl.lastIndexOf("/") + 1);
                destinationFileLocation = new File(qualifiedDownloadFilePath);
                readSourceFile.renameTo(destinationFileLocation);
                fileWriter = new FileWriter(readSourceFile);
                fileWriter.flush();

                // set download SUCCES message to return
                responseString = "downloaded successfully at " + qualifiedDownloadFilePath;
            }
        }
            catch(Exception ex) {
                ex.printStackTrace();
            }
            finally{
                // release resources, if any
                clientResponse.close();
                fileWriter.close();
                clientRequest.clear();
            }
        return responseString;
    }*/
   public static byte[] getBytesFromStream(InputStream is) throws IOException {

       int len;
       int size = 1024;
       byte[] buf;

       ByteArrayOutputStream bos = new ByteArrayOutputStream();
       buf = new byte[size];
       while((len = is.read(buf, 0, size)) != -1) {
           bos.write(buf, 0, len);
       }
       buf = bos.toByteArray();

       return buf;
   }
}