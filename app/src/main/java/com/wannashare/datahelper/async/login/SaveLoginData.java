package com.wannashare.datahelper.async.login;

import android.os.AsyncTask;

import com.wannashare.localdatastore.models.Session;
import com.wannashare.localdatastore.models.User;
import com.wannashare.datahelper.user.UserDataHelper;
import com.wannashare.utils.DaoEnum;

import java.sql.SQLException;

/**
 * Created by chaitanya on 9/20/15.
 */
public class SaveLoginData extends AsyncTask<Object, Object, Object> {
    User user;
    Session session;
    UserDataHelper userDataHelper;

    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p/>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *

     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */

    public SaveLoginData(User user,Session session) {
       this.user = user;
        this.session = session;
        try {
            userDataHelper = new UserDataHelper();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected Object doInBackground(Object... params) {

        user = userDataHelper.getUserDetailsFromFB();
        try {
            userDataHelper.saveToLocalDB(user, DaoEnum.USER,"userid",user.getUserid());
            userDataHelper.saveToLocalDB(session, DaoEnum.SESSION,"sessionid",String.valueOf(session.getSessionid()));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        userDataHelper.saveUserToServer(user);
        userDataHelper.saveSessionToServer(session);

        return null;
    }
}
