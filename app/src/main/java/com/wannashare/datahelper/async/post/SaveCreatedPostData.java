package com.wannashare.datahelper.async.post;

import android.os.AsyncTask;

import com.wannashare.datahelper.user.UserDataHelper;
import com.wannashare.localdatastore.models.NearbyPost;
import com.wannashare.localdatastore.models.Post;
import com.wannashare.utils.DaoEnum;

import java.sql.SQLException;

/**
 * Created by chaitanya on 9/20/15.
 */
public class SaveCreatedPostData extends AsyncTask<Object, Object, Object> {

    Post post;
    String[] imagesPath;
    UserDataHelper userDataHelper;


    public SaveCreatedPostData(Post post, String[] imagesPath) {
        this.post = post;
        this.imagesPath = imagesPath;

        try {
            userDataHelper = new UserDataHelper();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected Object doInBackground(Object... params) {
/*
        try {
            userDataHelper.savetoLocalDB(post, DaoEnum.POST);
        } catch (SQLException e) {
            e.printStackTrace();
        }*/
        userDataHelper.savePostToServer(post,imagesPath);


        return null;
    }
}
