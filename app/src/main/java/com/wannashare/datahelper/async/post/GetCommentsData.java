package com.wannashare.datahelper.async.post;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.j256.ormlite.stmt.UpdateBuilder;
import com.wannashare.datahelper.user.UserDataHelper;
import com.wannashare.localdatastore.models.NearbyPost;
import com.wannashare.localdatastore.models.NearbyPostComment;
import com.wannashare.localdatastore.models.UserPost;
import com.wannashare.utils.DaoEnum;
import com.wannashare.utils.IRestApiClient;

import java.sql.SQLException;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class GetCommentsData extends AsyncTask<Object, Object, Object> {

    Integer postid;
    IRestApiClient restClient;
    UserDataHelper userDataHelper;
    boolean sendBroadcast;
    Context serviceContext;
    Intent commentIntent;

    public GetCommentsData(int postid,IRestApiClient restClient,Context serviceContext,Intent commentIntent) throws SQLException {
       this(postid,restClient);
        this.serviceContext = serviceContext;
        this.commentIntent = commentIntent;
        sendBroadcast = true;

    }


    public GetCommentsData(int postid,IRestApiClient restClient) throws SQLException {
        this.postid= postid;
        this.restClient=restClient;
        userDataHelper = new UserDataHelper();
    }

    @Override
    protected Object doInBackground(Object... params) {

      restClient.getComments(String.valueOf(postid), new Callback<List<NearbyPostComment>>() {
          @Override
          public void success(List<NearbyPostComment> nearbyPostComments, Response response) {

              for (NearbyPostComment nearbyPostComment : nearbyPostComments) {
                  try {
                      userDataHelper.saveToLocalDB(nearbyPostComment, DaoEnum.NEARBYPOSTCOMMENT, "commentid", String.valueOf(nearbyPostComment.getCommentid()));
                  } catch (SQLException e) {
                      e.printStackTrace();
                  }
              }
              if(sendBroadcast){
                  UserPost userPost = restClient.getUserPostById(String.valueOf(postid));
                  try {

                      if (userDataHelper.isExistInDB(userPost, DaoEnum.NEARBYPOST, "postid", String.valueOf(userPost.getPost().getPostid()))) {

                          userDataHelper.updateInLocalDB("totallikes", userPost.getTotallikes(),DaoEnum.NEARBYPOST,"postid", String.valueOf(userPost.getPost().getPostid()));
                          userDataHelper.updateInLocalDB("totaldislikes", userPost.getTotaldisLikes(),DaoEnum.NEARBYPOST,"postid", String.valueOf(userPost.getPost().getPostid()));
                          userDataHelper.updateInLocalDB( "totalcomments", userPost.getTotalcomments(), DaoEnum.NEARBYPOST,"postid", String.valueOf(userPost.getPost().getPostid()));
                          userDataHelper.saveToLocalCache("updateUserFeed","true");
                      }
                  } catch (SQLException e1) {
                      e1.printStackTrace();
                  }
                  serviceContext.sendBroadcast(commentIntent);
              }
          }

          @Override
          public void failure(RetrofitError error) {

          }
      });


        return null;
    }
}
