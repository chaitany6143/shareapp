package com.wannashare.datahelper.async.post;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.wannashare.datahelper.user.UserDataHelper;
import com.wannashare.localdatastore.models.NearbyPost;
import com.wannashare.localdatastore.models.NearbyPostComment;
import com.wannashare.localdatastore.models.UserPost;
import com.wannashare.utils.DaoEnum;
import com.wannashare.utils.IRestApiClient;

import java.sql.SQLException;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by chaitanya on 10/8/15.
 */
public class GetUserPost extends AsyncTask<Object, Object, Object> {

    Integer postid;
    IRestApiClient restClient;
    UserDataHelper userDataHelper;



    public GetUserPost(int postid, IRestApiClient restClient) throws SQLException {
        this.postid = postid;
        this.restClient = restClient;
        userDataHelper = new UserDataHelper();
    }

    @Override
    protected Object doInBackground(Object... params) {


        UserPost userPost = restClient.getUserPostById(String.valueOf(postid));
                try {

                    if (userDataHelper.isExistInDB(userPost, DaoEnum.NEARBYPOST, "postid", String.valueOf(userPost.getPost().getPostid()))) {
                       // userDataHelper.updateInLocalDB("postid", String.valueOf(userPost.getPost().getPostid()), DaoEnum.NEARBYPOST, "totallikes", String.valueOf(userPost.getTotallikes()));
                        //userDataHelper.updateInLocalDB("postid", String.valueOf(userPost.getPost().getPostid()), DaoEnum.NEARBYPOST, "totaldisLikes", String.valueOf(userPost.getTotaldisLikes()));
                        //userDataHelper.updateInLocalDB("postid", String.valueOf(userPost.getPost().getPostid()), DaoEnum.NEARBYPOST, "totalcomments", String.valueOf(userPost.getTotalcomments()));

                    }
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

        return null;
    }
}