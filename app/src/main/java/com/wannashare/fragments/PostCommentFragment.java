package com.wannashare.fragments;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.wannashare.R;
import com.wannashare.activities.MainActivity;
import com.wannashare.adapters.CommentListAdapter;
import com.wannashare.datahelper.user.UserDataHelper;
import com.wannashare.localdatastore.models.NearbyPostComment;
import com.wannashare.utils.Constants;
import com.wannashare.utils.DaoEnum;

import java.sql.SQLException;
import java.util.List;

public class PostCommentFragment extends BaseFragment {

    private static final String TAG = Constants.APP_TAG;
    private ListView listView;
    private CommentListAdapter listAdapter;
    private UserDataHelper userDataHelper;
    List<NearbyPostComment> nearbyPostComments;

    public PostCommentFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_comment, container, false);

        listView = (ListView) rootView.findViewById(R.id.comments);

        String postid = getArguments().getString("postid");

        try {
            userDataHelper = new UserDataHelper();
            nearbyPostComments = (List<NearbyPostComment>) userDataHelper.getFromLocalDbByField("postid",postid, DaoEnum.NEARBYPOSTCOMMENT);
        } catch (SQLException e) {
            e.printStackTrace();
        }

                listAdapter = new CommentListAdapter(getActivity(), nearbyPostComments,userDataHelper);
                listView.setAdapter(listAdapter);
                listAdapter.notifyDataSetChanged();



      //  ((MainActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#3b5998")));
        ((MainActivity)getActivity()).getSupportActionBar().setIcon(
                new ColorDrawable(getResources().getColor(android.R.color.transparent)));

       // feedItems = ((MainApplication) ApplicationContextHolder.getApplicationContext()).getUserService().getUserFeedItems();



        return rootView;
    }



}
