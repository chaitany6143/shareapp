package com.wannashare.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.wannashare.R;
import com.wannashare.activities.PostActivity;
import com.wannashare.application.MainApplication;
import com.wannashare.datahelper.user.UserDataHelper;
import com.wannashare.localdatastore.models.NearbyPost;
import com.wannashare.localdatastore.models.User;
import com.wannashare.utils.Constants;
import com.wannashare.utils.DaoEnum;
import com.wannashare.utils.MapUtil;
import com.wannashare.wrappers.ContextStore;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class MapFragment extends Fragment {


    MapView mMapView;
    Circle mapCircle;
    float lastRadius;
    public GoogleMap googleMap;
    private boolean hasSetUpInitialLocation;
    private int mostRecentMapUpdate;
    LatLng currentLocation;
    Location lastKnownLocation;
    UserDataHelper userDataHelper;
    private final Map<String, Marker> mapMarkers = new HashMap<String, Marker>();
    public static float radius = Float.parseFloat(((MainApplication) ContextStore.getInstance().getApplicationContext()).getLocalCacheAccessor().getFromCache(Constants.SEARCH_DISTANCE));



        public MapFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        mMapView = (MapView) rootView.findViewById(R.id.mapView);

        mMapView.onCreate(savedInstanceState);
        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        googleMap = mMapView.getMap();
        googleMap.setMyLocationEnabled(true);
        try {
            userDataHelper = new UserDataHelper();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        rootView.findViewById(R.id.post_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivity(), PostActivity.class));
            }
        });

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

    }



    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.LOCATION_BROADCAST_ACTION);
        getActivity().
                registerReceiver(receiver, intentFilter);
        if(mMapView!=null)
            mMapView.onResume();



        if (currentLocation != null) {

            MapUtil.updateZoom(currentLocation, googleMap);
            // }
            updateCircle(currentLocation);

        lastRadius = radius;

        try {
            renderMarkersOnMap(currentLocation);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        }

    }

    @Override
    public void onPause() {

        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receiver);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receiver);
        super.onDestroy();
        if(mMapView!=null)
            mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if(mMapView!=null)
            mMapView.onLowMemory();
    }


    public void renderMarkersOnMap(LatLng latLng) throws SQLException {

        final int myUpdateNumber = ++mostRecentMapUpdate;

        // If location info isn't available, clean up any existing markers
        if (currentLocation == null) {
            removeMarkersFromMap(new HashSet<String>());
            return;
        }
       List<NearbyPost> nearbyPostList = (List<NearbyPost>) userDataHelper.getAllFromLocalDb(DaoEnum.NEARBYPOST);

            if (myUpdateNumber != mostRecentMapUpdate)
                    return;

                // Posts to show on the map
                Set<String> toKeep = new HashSet<String>();

                for (NearbyPost userPost : nearbyPostList) {

                   User user = (User)((List<User>) userDataHelper.getFromLocalDbByField("userid",userPost.getUserid(),DaoEnum.USER)).get(0);
                    String userName = user.getName();
                   // ImageManager.downloadIfNotExist(userPost);

                    // Add this post to the list of map pins to keep
                    toKeep.add(String.valueOf(userPost.getPostid()));
                    // Check for an existing marker for this post
                    Marker oldMarker = mapMarkers.get(userPost.getPostid());

                    // Set up the map marker's location
                    MarkerOptions markerOpts =
                            new MarkerOptions().position(new LatLng(userPost.getLatitude(), userPost.getLongitude()));

                    Location postLocation = new Location("");
                    postLocation.setLatitude(userPost.getLatitude());
                    postLocation.setLongitude(userPost.getLongitude());
                    Location userLocation = new Location("");
                    userLocation.setLatitude(currentLocation.latitude);
                    userLocation.setLongitude(currentLocation.longitude);
                    if (postLocation.distanceTo(userLocation) > (radius * Constants.METERS_PER_FEET)) {

                        if (oldMarker != null) {
                            if (oldMarker.getSnippet() == null) {
                                // Out of range marker already exists, skip adding it
                                continue;
                            } else {
                                // Marker now out of range, needs to be refreshed
                                oldMarker.remove();
                            }
                        }
                        // Display a red marker with a predefined title and no snippet
                        markerOpts =
                                markerOpts.title("Can\\'t view post! Get closer.").icon(
                                        BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                    } else {
                        // Check for an existing in range marker
                        if (oldMarker != null) {
                            if (oldMarker.getSnippet() != null) {
                                // In range marker already exists, skip adding it
                                continue;
                            } else {
                                // Marker now in range, needs to be refreshed
                                oldMarker.remove();
                            }
                        }
                        // Display a green marker with the post information
                        markerOpts =
                                markerOpts.title(userPost.getPost()).snippet(userName)
                                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                    }
                    // Add a new marker
                    Marker marker = googleMap.addMarker(markerOpts);
                    mapMarkers.put(String.valueOf(userPost.getPostid()), marker);

                }
                // Clean up old markers.
                removeMarkersFromMap(toKeep);


    }

    private void removeMarkersFromMap(Set<String> markersToKeep) {
        for (String objId : new HashSet<String>(mapMarkers.keySet())) {
            if (!markersToKeep.contains(objId)) {
                Marker marker = mapMarkers.get(objId);
                marker.remove();
                mapMarkers.get(objId).remove();
                mapMarkers.remove(objId);
            }
        }
    }

    public void updateCircle(LatLng myLatLng) {
        if (mapCircle == null) {
            mapCircle =
                    googleMap.addCircle(
                            new CircleOptions().center(myLatLng).radius(radius * Constants.METERS_PER_FEET));
            int baseColor = Color.DKGRAY;
            mapCircle.setStrokeColor(baseColor);
            mapCircle.setStrokeWidth(2);
            mapCircle.setFillColor(Color.argb(50, Color.red(baseColor), Color.green(baseColor),
                    Color.blue(baseColor)));
        }
        mapCircle.setCenter(myLatLng);
        mapCircle.setRadius(radius * Constants.METERS_PER_FEET); // Convert radius in feet to meters.
    }


    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            currentLocation = new LatLng(intent.getDoubleExtra("Latitude",0.0),intent.getDoubleExtra("Longitude",0.0));
            if (!hasSetUpInitialLocation) {
                // Zoom to the current location.
                MapUtil.updateZoom(currentLocation,googleMap);
                hasSetUpInitialLocation = true;
            }

            updateCircle(currentLocation);

            try {
                renderMarkersOnMap(currentLocation);
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    };

    }
