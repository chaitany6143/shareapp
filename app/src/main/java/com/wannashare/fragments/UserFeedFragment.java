package com.wannashare.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.reflect.TypeToken;
import com.wannashare.R;
import com.wannashare.activities.MainActivity;
import com.wannashare.adapters.FeedListAdapter;
import com.wannashare.application.MainApplication;
import com.wannashare.datahelper.user.UserDataHelper;
import com.wannashare.localdatastore.models.NearbyPost;
import com.wannashare.localdatastore.models.NearbyPostComment;
import com.wannashare.localdatastore.models.UserPost;
import com.wannashare.utils.Constants;
import com.wannashare.utils.DaoEnum;
import com.wannashare.utils.IRestApiClient;
import com.wannashare.utils.MapUtil;
import com.wannashare.wrappers.ContextStore;


import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.List;


public class UserFeedFragment extends BaseFragment {


    private ListView listView;
    private FeedListAdapter listAdapter;
    private UserDataHelper userDataHelper;
    private List<NearbyPost> nearbyPosts;

    public UserFeedFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_userfeed, container, false);

        listView = (ListView) rootView.findViewById(R.id.feedlist);

        try {
            userDataHelper = new UserDataHelper();
            nearbyPosts = (List<NearbyPost>) userDataHelper.getAllFromLocalDb(DaoEnum.NEARBYPOST);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        listAdapter = new FeedListAdapter(getActivity(), nearbyPosts);
        listView.setAdapter(listAdapter);


        return rootView;
    }

    @Override
    public void onResume() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.POST_BROADCAST_ACTION);
        getActivity().
                registerReceiver(receiver, intentFilter);

        if (userDataHelper.getFromLocalCache("updateUserFeed") != null) {
            if (userDataHelper.getFromLocalCache("updateUserFeed").equals("true")) {
                try {
                    userDataHelper = new UserDataHelper();
                    nearbyPosts = (List<NearbyPost>) userDataHelper.getAllFromLocalDb(DaoEnum.NEARBYPOST);
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                listAdapter = new FeedListAdapter(getActivity(), nearbyPosts);
                listView.setAdapter(listAdapter);
                userDataHelper.saveToLocalCache("updateUserFeed", "false");
            }

        }


        //  if(localCacheAccessor.getFromCache("userFeed")!=null){

        //    Type listOfTestObject = new TypeToken<List<UserPost>>(){}.getType();
        //  List<UserPost> userPostList = gson.fromJson(localCacheAccessor.getFromCache("userFeed"),listOfTestObject);
        //if(userPostList!=null&&userPostList.size()>0) {
        //  UserPost user = userPostList.get(0);
        //  String userName = user.getUser().getName();
        // }


        //}
      /*  else{
            RestAdapter restAdapter = ((MainApplication) ContextStore.getApplicationContext()).getRestAdapter();
            IRestApiClient restClient = restAdapter.create(IRestApiClient.class);
            restClient.getNearbyPosts(new Callback<List<UserPost>>() {
                *//**
         * Successful HTTP response.
         *
         * @param userPosts
         * @param response
         *//*
                @Override
                public void success(List<UserPost> userPosts, Response response) {
                    listAdapter = new FeedListAdapter(getActivity(), userPosts);
                    listView.setAdapter(listAdapter);
                }

                *//**
         * Unsuccessful HTTP response due to network failure, non-2XX status code, or unexpected
         * exception.
         *
         * @param error
         *//*
                @Override
                public void failure(RetrofitError error) {
                    //To do !!!
                }
            });
        }*/


        //  ((MainActivity)getActivity()).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#3b5998")));
        ((MainActivity) getActivity()).getSupportActionBar().setIcon(
                new ColorDrawable(getResources().getColor(android.R.color.transparent)));

        // feedItems = ((MainApplication) ApplicationContextHolder.getApplicationContext()).getUserService().getUserFeedItems();
        if (listAdapter != null)
            listAdapter.notifyDataSetChanged();
        super.onResume();
    }

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                userDataHelper = new UserDataHelper();
                nearbyPosts = (List<NearbyPost>) userDataHelper.getAllFromLocalDb(DaoEnum.NEARBYPOST);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            listAdapter = new FeedListAdapter(getActivity(), nearbyPosts);
            listView.setAdapter(listAdapter);
            listAdapter.notifyDataSetChanged();
        }
    };

    @Override
    public void onPause() {

        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receiver);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receiver);
        super.onDestroy();
    }
}
