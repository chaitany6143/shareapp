package com.wannashare.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wannashare.activities.MainActivity;
import com.wannashare.application.MainApplication;
import com.wannashare.wrappers.ContextStore;
import com.wannashare.localdatastore.LocalCacheAccessor;

/**
 * Created by chaitanya on 8/16/15.
 */
public class BaseFragment extends Fragment {
    public MainActivity mainActivity;
    protected LocalCacheAccessor localCacheAccessor = ((MainApplication) ContextStore.getApplicationContext()).getLocalCacheAccessor();
    protected Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            .create();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mainActivity		=	(MainActivity)getActivity();
    }

    public boolean onBackPressed(){
        return false;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){

    }
}