package com.wannashare.localdatastore;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Set;

/**
 * Created by chaitanya on 7/8/15.
 */
public class LocalCacheAccessor {

    SharedPreferences sharedPreferences;
    Gson jsonBuilder;
    Context context;

/* Called in the application class with its context. Can be used through out the application */
    public LocalCacheAccessor(Context context){
        this.context = context;
        sharedPreferences = context.getSharedPreferences("app_local_data",context.MODE_PRIVATE);


    }
    public void persistToCache(String key,String value){

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key,value);
        editor.apply();

    }

    public String getFromCache(String key){


       return sharedPreferences.getString(key,null);


    }

    public void removeFromCache(String key){


        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(key);
        editor.apply();


    }


}
