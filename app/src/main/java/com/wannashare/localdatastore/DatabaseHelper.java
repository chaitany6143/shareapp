package com.wannashare.localdatastore;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.wannashare.localdatastore.models.NearbyPost;
import com.wannashare.localdatastore.models.NearbyPostImage;
import com.wannashare.localdatastore.models.Post;
import com.wannashare.localdatastore.models.NearbyPostComment;
import com.wannashare.localdatastore.models.Session;
import com.wannashare.localdatastore.models.User;
import com.wannashare.wrappers.ContextStore;


public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    public static final String USER_MOVIE_REVIEW_TABLE = "movie_review_user";
    public static final String DATABASE_NAME = "wannasharedb";
    public static final int DATABASE_VERSION = 3;


    private static DatabaseHelper mInstance = null;
    public SQLiteDatabase mDB = null;

    public DatabaseHelper(Context context, String databaseName, CursorFactory factory, int dbVersion) {
        super(context, databaseName, factory, dbVersion);
        mDB = getWritableDatabase();

    }
    public static DatabaseHelper getInstance() {
        if (mInstance == null) {
            mInstance = new DatabaseHelper(ContextStore.getInstance().getApplicationContext(), DATABASE_NAME, null, DATABASE_VERSION);
        }
        return mInstance;
    }
    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onCreate");
            TableUtils.createTable(connectionSource, Session.class);
            TableUtils.createTable(connectionSource, User.class);
            TableUtils.createTable(connectionSource, NearbyPost.class);
            TableUtils.createTable(connectionSource, NearbyPostImage.class);
            TableUtils.createTable(connectionSource, Post.class);
            TableUtils.createTable(connectionSource, NearbyPostComment.class);
            System.out.println();
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
            throw new RuntimeException(e);
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onUpgrade");
            TableUtils.createTable(connectionSource, Session.class);
            TableUtils.createTable(connectionSource, User.class);
            TableUtils.createTable(connectionSource, NearbyPost.class);
            TableUtils.createTable(connectionSource, NearbyPostImage.class);
            TableUtils.createTable(connectionSource, Post.class);
            TableUtils.createTable(connectionSource, NearbyPostComment.class);
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't drop databases", e);
            throw new RuntimeException(e);
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        super.close();
    }


    /*@Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    public SQLiteDatabase getDb() {
        return mDB;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
       *//* final String[] creatStatments = new String[]{
                "create table IF NOT EXISTS " + USER_TABLE +
                        "(" + User.COLUMD_USER_ID + " INTEGER PRIMARY KEY," + User.COLUMN_USER_NAME + " TEXT NOT NULL, " + User.COLUMN_PASSWORD + " TEXT NOT NULL )",
                "create table  IF NOT EXISTS " + USER_SESSION_TABLE + "( " + UserSession.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " + UserSession.COLUMN_SESSION_TOKEN + " TEXT NOT NULL )",
                "create table  IF NOT EXISTS " + USER_INFO_TABLE + "( " + UserDetails.COLUMN_USER_NAME + " TEXT NOT NULl, " + UserDetails.COLUMN_FIRST_NAME + " TEXT NOT NULL, " + UserDetails.COLUMN_MIDDLE_NAME + " TEXT, " + UserDetails.COLUMN_LAST_NAME + " TEXT NOT NULL, " + UserDetails.COLUMN_EMAIL_ADDRES + " TEXT )",
                "CREATE TABLE  IF NOT EXISTS " + MOVIES_TABLE + " ( movie_id INTEGER PRIMARY KEY, title TEXT NOT NULL, year TEXT, rated TEXT, released BIGINT, runtime TEXT, genre TEXT, director TEXT, writer TEXT, actors TEXT, plot TEXT, simple_plot TEXT, language TEXT, country TEXT, awards TEXT, poster TEXT, metascore TEXT, filming_locations TEXT, url_imdb TEXT NOT NULL, imdb_rating TEXT, imdb_votes TEXT, imdb_id TEXT, type TEXT, tomato_meter TEXT, tomato_image TEXT, tomato_rating TEXT, tomato_reviews TEXT, tomato_fresh TEXT, tomato_rotten TEXT, tomato_consensus TEXT, tomato_user_meter TEXT, tomato_user_rating TEXT, tomato_user_reviews TEXT, dvd TEXT, boxoffice TEXT, website TEXT, production TEXT, poster_sdcard TEXT, poster_data BLOB, trailer_youtube_video_id TEXT, related_videos TEXT, movie_songs TEXT , delete_flag INTEGER DEFAULT 0)",
                "CREATE TABLE  IF NOT EXISTS " + USER_WATCHED_MOVIE_TABLE + "( " + UserWatchedMovie.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + UserWatchedMovie.COLUMN_MOVIE_ID + " INTEGER, " + UserWatchedMovie.COLUMN_WATCHED + " INTEGER DEFAULT 0)",
//                "CREATE TABLE " + USER_RECOMMEND_MOVIE_TABLE + "( " + UserRecommendMovie.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + UserRecommendMovie.COLUMN_MOVIE_ID + " INTEGER, " + UserRecommendMovie.COLUMN_VOTE_STAUTS + " INTEGER DEFAULT 0)",
                "CREATE TABLE  IF NOT EXISTS " + USER_FRIENDS_RECOMMNEDED_MOVIE_TABLE + "( " + UserFriendRecommendMovie.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + UserFriendRecommendMovie.COLUMN_MOVIE_ID + " INTEGER, " + UserFriendRecommendMovie.COLUMN_VOTE_STAUTS + " INTEGER DEFAULT 0, " + UserFriendRecommendMovie.COLUMN_FRIEND_USER_NAME + " TEXT)",
                "CREATE TABLE  IF NOT EXISTS " + USER_MOVIE_REVIEW_TABLE + "( " + UserMovieReviewData.COLUMN_REVIEW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + UserMovieReviewData.COLUMN_MOVIE_ID + " INTEGER, " + UserMovieReviewData.COLUMN_USER_NAME + " TEXT, " + UserMovieReviewData.COLUMN_RATING + " REAL, " + UserMovieReviewData.COLUMN_TITLE + " TEXT, " + UserMovieReviewData.COLUMN_REVIEW + " TEXT, " + UserMovieReviewData.COLUM_VOTE + " INTEGER DEFAULT 0)",
                "CREATE TABLE  IF NOT EXISTS " + MOVIE_RECOMMENDATION_USER + "( " + MovieRecommendationsUser.COLUMD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + MovieRecommendationsUser.COLUMN_MOVIE_ID + " INTEGER)",
                "CREATE TABLE  IF NOT EXISTS " + ALL_MOVIES_TABLE + "( " + AllMovies.COLUMN_MOVIE_ID + " INTEGER, " + AllMovies.COLUMN_TITLE + " Text, " + AllMovies.COLUMN_RELEASED + " BIGINT, " + AllMovies.COLUMN_DELETE_FLAG + " INTEGER DEFAULT 0 )",
                "CREATE TABLE  IF NOT EXISTS " + USER_SYNC_STATUS + "( " + UserSyncStatus.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + UserSyncStatus.COLUMN_LAST_MOVIE_ID + " INTEGER, " + UserSyncStatus.COLUMN_LAST_MOVIE_RELEASED_DATE + " BIGINT, " + UserSyncStatus.COLUMB_LAST_SYNC_TIME + " BIGINT)",
                "CREATE TABLE  IF NOT EXISTS " + MOVIE_SYNC_TIME + "( " + MovieSyncTime.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + MovieSyncTime.COLUMN_MOVIE_DATA_SYNC_TIME + " BIGINT)"

        };

        for (String sStmt : creatStatments) {
            db.execSQL(sStmt);
        }  *//*

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        System.out.println("oldVersion: " + oldVersion);
        System.out.println("newVersion: " + newVersion);
        try {

            switch (oldVersion) {
                case 1:
                    String alterMoviesAddRelatedVideos = "ALTER TABLE " + MOVIES_TABLE + " ADD COLUMN related_videos TEXT;";
                    db.execSQL(alterMoviesAddRelatedVideos);
                case 2:
                    String alterMoviesAddMovies = "ALTER TABLE " + MOVIES_TABLE + " ADD COLUMN movie_songs TEXT;";
                    db.execSQL(alterMoviesAddMovies);
                    String alterMoviesDeleteFalg = "ALTER TABLE " + MOVIES_TABLE + " ADD COLUMN delete_flag INTEGER DEFAULT 0";
                    db.execSQL(alterMoviesDeleteFalg);
                    String alterAllMoviesDeleteFalg = "ALTER TABLE " + ALL_MOVIES_TABLE + " ADD COLUMN delete_flag INTEGER DEFAULT 0";
                    db.execSQL(alterAllMoviesDeleteFalg);
                    break;
                default:
                    onCreate(db);
                    break;
            }

        } catch (Exception e) {

        }
        String deleteSession = "delete from " + MOVIE_SYNC_TIME + ";";
        db.execSQL(deleteSession);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
    }*/
}