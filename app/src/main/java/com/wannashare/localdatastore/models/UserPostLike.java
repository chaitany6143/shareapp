package com.wannashare.localdatastore.models;

public class UserPostLike {


    int postid;
    String userid;
    int action;


    public int getPostid() {
        return postid;
    }
    public void setPostid(int postid) {
        this.postid = postid;
    }
    public String getUserid() {
        return userid;
    }
    public void setUserid(String userid) {
        this.userid = userid;
    }
    public int getAction() {
        return action;
    }
    public void setAction(int action) {
        this.action = action;
    }



}