package com.wannashare.localdatastore.models;



/**
 
 * @author Chaitanya
 *
 */

public class UserPostImage {



	private int imageid;

	private String postid;

	private String image;

	public int getImageid() {
		return imageid;
	}

	public void setImageid(int imageid) {
		this.imageid = imageid;
	}

	public String getPostid() {
		return postid;
	}

	public void setPostid(String postid) {
		this.postid = postid;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "UserPostImage [imageid=" + imageid + ", postid=" + postid
				+ ", image=" + image + "]";
	}

	
	
	
	
	
}
