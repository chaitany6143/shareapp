package com.wannashare.localdatastore.models;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "app_user_post_comment")
public class NearbyPostComment {

	private static final long serialVersionUID = -222864131214757024L;

	@DatabaseField(columnName = "commentid")
	private int commentid;
	@DatabaseField(columnName = "postid")
	private int postid;
	@DatabaseField(columnName = "comment")
	private String comment;
	@DatabaseField(columnName = "userid")
	private String userid;
	@DatabaseField(columnName = "timestamp")
	private String timestamp;
	
	
	public int getCommentid() {
		return commentid;
	}
	public void setCommentid(int commentid) {
		this.commentid = commentid;
	}
	public int getPostid() {
		return postid;
	}
	public void setPostid(int postid) {
		this.postid = postid;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

}
