package com.wannashare.localdatastore.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by chaitanya on 9/13/15.
 */
@DatabaseTable(tableName = "app_user_post_image")
public class NearbyPostImage implements Serializable {

    private static final long serialVersionUID = -222864131214757024L;

    @DatabaseField(columnName = "postid",id=true)
    private int postid;

    public String getThumbnailLocation() {
        return thumbnailLocation;
    }

    public void setThumbnailLocation(String thumbnailLocation) {
        this.thumbnailLocation = thumbnailLocation;
    }

    @DatabaseField(columnName = "thumbnaillocation")
    private String thumbnailLocation;
    @DatabaseField(columnName = "actuallocation")
    private String actuallocation;



    public String getActuallocation() {
        return actuallocation;
    }

    public void setActuallocation(String actuallocation) {
        this.actuallocation = actuallocation;
    }


    public int getPostid() {
        return postid;
    }

    public void setPostid(int postid) {
        this.postid = postid;
    }


}
