package com.wannashare.localdatastore.models;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by chaitanya on 8/5/15.
 */
public class UserPost {
    User user;
    Post post;
    List<String> thumbnaillocation;
    List<String> actuallocation;
    int totallikes;
    int totaldisLikes;
    int totalcomments;


    public int getTotallikes() {
        return totallikes;
    }

    public void setTotallikes(int totallikes) {
        this.totallikes = totallikes;
    }

    public List<String> getThumbnaillocation() {
        return thumbnaillocation;
    }

    public void setThumbnaillocation(List<String> thumbnaillocation) {
        this.thumbnaillocation = thumbnaillocation;
    }

    public List<String> getActuallocation() {
        return actuallocation;
    }

    public void setActuallocation(List<String> actuallocation) {
        this.actuallocation = actuallocation;
    }

    public int getTotaldisLikes() {
        return totaldisLikes;
    }

    public void setTotaldisLikes(int totaldisLikes) {
        this.totaldisLikes = totaldisLikes;
    }

    public int getTotalcomments() {
        return totalcomments;
    }

    public void setTotalcomments(int totalcomments) {
        this.totalcomments = totalcomments;
    }



    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
    public Post getPost() {
        return post;
    }
    public void setPost(Post post) {
        this.post = post;
    }
}
