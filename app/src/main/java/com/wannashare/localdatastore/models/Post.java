package com.wannashare.localdatastore.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = "app_user_post")
public class Post implements Serializable{

	private static final long serialVersionUID = -222864131214757024L;

	@DatabaseField(columnName = "userid")
	private String userid;

	@DatabaseField(columnName = "postid")
	private int postid;

	@DatabaseField(columnName = "post")
	private String post;

	@DatabaseField(columnName = "latitude")
	private double latitude;

	@DatabaseField(columnName = "longitude")
	private double longitude;

	@DatabaseField(columnName = "timestamp")
	private String timestamp;







	public int getPostid() {
		return postid;
	}


	public void setPostid(int postid) {
		this.postid = postid;
	}


	public String getPost() {
		return post;
	}


	public void setPost(String post) {
		this.post = post;
	}


	public String getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longtitude) {
		this.longitude = longtitude;
	}


	public String getUserid() {
		return userid;
	}


	public void setUserid(String userid) {
		this.userid = userid;
	}




}
