package com.wannashare.localdatastore.models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * Created by chaitanya on 9/13/15.
 */
@DatabaseTable(tableName = "app_nearby_post")
public class NearbyPost implements Serializable{

    private static final long serialVersionUID = -222864131214757024L;

    @DatabaseField(columnName = "userid")
    private String userid;

    @DatabaseField(columnName = "postid",id=true)
    private int postid;

    @DatabaseField(columnName = "post")
    private String post;

    @DatabaseField(columnName = "latitude")
    private double latitude;

    @DatabaseField(columnName = "longitude")
    private double longitude;

    @DatabaseField(columnName = "timestamp")
    private String timestamp;

    @DatabaseField(columnName = "totallikes")
    private int totallikes;

    @DatabaseField(columnName = "totaldislikes")
    private int totaldislikes;
    @DatabaseField(columnName = "totalcomments")
    private int totalcomments;



    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public int getPostid() {
        return postid;
    }

    public void setPostid(int postid) {
        this.postid = postid;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getTotallikes() {
        return totallikes;
    }

    public void setTotallikes(int totallikes) {
        this.totallikes = totallikes;
    }

    public int getTotaldislikes() {
        return totaldislikes;
    }

    public void setTotaldislikes(int totaldislikes) {
        this.totaldislikes = totaldislikes;
    }

    public int getTotalcomments() {
        return totalcomments;
    }

    public void setTotalcomments(int totalcomments) {
        this.totalcomments = totalcomments;
    }





}
