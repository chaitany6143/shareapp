package com.wannashare.localdatastore.models;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * @author Chaitanya
 */
@DatabaseTable(tableName = "app_session")
public class Session implements Serializable {

    private static final long serialVersionUID = -222864131214757024L;

    @DatabaseField(columnName = "sessionid",generatedId = true)
    int sessionid;
    @DatabaseField(columnName = "userid")
    String userid;
    @DatabaseField(columnName = "token")
    String token;

    public int getSessionid() {
        return sessionid;
    }

    public void setSessionid(int sessionid) {
        this.sessionid = sessionid;
    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }


}
