package com.wannashare.localdatastore.models;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * @author Chaitanya
 */

@DatabaseTable(tableName = "app_user")
public class User implements Serializable{


    private static final long serialVersionUID = -222864131214757024L;


    @DatabaseField(columnName = "userid",id = true)
    private String userid;

    @DatabaseField(columnName = "profileid")
    private String profileid;

    @DatabaseField(columnName = "name")
    private String name;

    @DatabaseField(columnName = "location")
    private String location;

    @DatabaseField(columnName = "gender")
    private String gender;

    @DatabaseField(columnName = "birthday")
    private String birthday;


    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getProfileid() {
        return profileid;
    }

    public void setProfileid(String profileid) {
        this.profileid = profileid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }


}
