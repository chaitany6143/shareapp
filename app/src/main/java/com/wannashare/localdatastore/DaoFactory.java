package com.wannashare.localdatastore;

/**
 * Created by chaitanya on 9/7/15.
 */

import android.database.SQLException;

import com.j256.ormlite.android.AndroidConnectionSource;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.wannashare.localdatastore.models.NearbyPost;
import com.wannashare.localdatastore.models.NearbyPostImage;
import com.wannashare.localdatastore.models.Post;
import com.wannashare.localdatastore.models.NearbyPostComment;
import com.wannashare.localdatastore.models.Session;
import com.wannashare.localdatastore.models.User;
import com.wannashare.utils.DaoEnum;

public class DaoFactory {


    private ConnectionSource connectionSource = null;
    private Dao<Session, String> sessionDao = null;
    private Dao<User, String> userDao = null;
    private Dao<Post, String> postDao = null;
    private Dao<NearbyPost, String> nearbyPostDao = null;
    private Dao<NearbyPostImage, String> nearbyPostImageDao = null;


    private Dao<NearbyPostComment, String> postCommentDao = null;
    private static DaoFactory instance = null;

    public Dao<NearbyPostComment, String> getPostCommentDao() {
        return postCommentDao;
    }

    public void setPostCommentDao(Dao<NearbyPostComment, String> postCommentDao) {
        this.postCommentDao = postCommentDao;
    }

    public Dao<NearbyPostImage, String> getNearbyPostImageDao() {
        return nearbyPostImageDao;
    }

    public void setNearbyPostImageDao(Dao<NearbyPostImage, String> nearbyPostImageDao) {
        this.nearbyPostImageDao = nearbyPostImageDao;
    }

    public Dao<NearbyPost, String> getNearbyPostDao() {
        return nearbyPostDao;
    }

    public void setNearbyPostDao(Dao<NearbyPost, String> nearbyPostDao) {
        this.nearbyPostDao = nearbyPostDao;
    }


    public Dao<User, String> getUserDao() {
        return userDao;
    }

    public Dao<Session, String> getSessionDao() {
        return sessionDao;
    }

    public Dao<Post, String> getPostDao() {
        return postDao;
    }

    public static DaoFactory getInstance() throws java.sql.SQLException {
        if(instance==null) {
            instance = new DaoFactory();
        }
        return instance;
    }


    private DaoFactory() throws java.sql.SQLException {
        connectionSource = new AndroidConnectionSource(DatabaseHelper.getInstance());
        try {
            sessionDao = BaseDaoImpl.createDao(connectionSource, Session.class);
            userDao = BaseDaoImpl.createDao(connectionSource, User.class);
            nearbyPostDao = BaseDaoImpl.createDao(connectionSource, NearbyPost.class);
            nearbyPostImageDao = BaseDaoImpl.createDao(connectionSource, NearbyPostImage.class);
            postDao = BaseDaoImpl.createDao(connectionSource,Post.class);
            postCommentDao = BaseDaoImpl.createDao(connectionSource,NearbyPostComment.class);
           // postDao = BaseDaoImpl.createDao(connectionSource, Post.class);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Dao<?,?> getDao(DaoEnum daoEnum){
        if(daoEnum.equals(DaoEnum.SESSION)){
            return sessionDao;
        }
        else if(daoEnum.equals(DaoEnum.USER)){
            return userDao;
        }
        else if(daoEnum.equals(DaoEnum.NEARBYPOST)){
            return nearbyPostDao;
        }
        else if(daoEnum.equals(DaoEnum.NEARBYPOSTIMAGE)){
            return nearbyPostImageDao;
        }
        else if(daoEnum.equals(DaoEnum.POST)){
            return postDao;
        }
        else if(daoEnum.equals(DaoEnum.NEARBYPOSTCOMMENT)){
            return postCommentDao;
        }

        return null;
    }
}
