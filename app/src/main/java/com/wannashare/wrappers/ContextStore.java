package com.wannashare.wrappers;

import android.content.Context;

import com.wannashare.utils.ActivityEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by chaitanya on 6/29/15.
 */
public class ContextStore {

    static Context applicationContext;

    private static ContextStore instance = null;


    private Map<ActivityEnum, Context> contextMap = null;


    private ContextStore() {
        contextMap = new HashMap<>();
    }



    public synchronized Context getContext(ActivityEnum activityEnum) {
        return contextMap.get(activityEnum);
    }

    public static ContextStore getInstance() {
        if (instance == null) {
            instance = new ContextStore();
        }

        return instance;
    }

    public synchronized void setContext(ActivityEnum activityEnum, Context baseContext) {
        contextMap.put(activityEnum, baseContext);
    }


    public static Context getApplicationContext() {
        return applicationContext;
    }

    public static void setApplicationContext(Context ac) {
        applicationContext = ac;
    }





}
