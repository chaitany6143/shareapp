package com.wannashare.wrappers;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.wannashare.R;
import com.wannashare.activities.MainActivity;
import com.wannashare.adapters.NavDrawerListAdapter;
import com.wannashare.fragments.CommunityFragment;
import com.wannashare.fragments.FindPeopleFragment;
import com.wannashare.fragments.MapFragment;
import com.wannashare.fragments.PagesFragment;
import com.wannashare.fragments.PhotosFragment;
import com.wannashare.models.NavDrawerItem;


/*
import java.util.ArrayList;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import activities.HomePageActivity;
import fragments.MapFragment;
import models.NavDrawerItem;*/


/**
 * Created by chaitanya on 5/22/15.
 */
public class DeleteNavigationDrawer {

    Context context;

    private ActionBarDrawerToggle mDrawerToggle;

    private DrawerLayout mDrawerLayout;

    // nav drawer title
    private CharSequence mDrawerTitle;

    // slide menu items
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;

    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;



    private ListView mDrawerList;

    MainActivity homePage;

    public DrawerLayout getmDrawerLayout() {
        return mDrawerLayout;
    }

    public void setmDrawerLayout(DrawerLayout mDrawerLayout) {
        this.mDrawerLayout = mDrawerLayout;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ActionBarDrawerToggle getmDrawerToggle() {
        return mDrawerToggle;
    }

    public void setmDrawerToggle(ActionBarDrawerToggle mDrawerToggle) {
        this.mDrawerToggle = mDrawerToggle;
    }

    public CharSequence getmDrawerTitle() {
        return mDrawerTitle;
    }

    public void setmDrawerTitle(CharSequence mDrawerTitle) {
        this.mDrawerTitle = mDrawerTitle;
    }

    public String[] getNavMenuTitles() {
        return navMenuTitles;
    }

    public void setNavMenuTitles(String[] navMenuTitles) {
        this.navMenuTitles = navMenuTitles;
    }

    public TypedArray getNavMenuIcons() {
        return navMenuIcons;
    }

    public void setNavMenuIcons(TypedArray navMenuIcons) {
        this.navMenuIcons = navMenuIcons;
    }

    public ArrayList<NavDrawerItem> getNavDrawerItems() {
        return navDrawerItems;
    }

    public void setNavDrawerItems(ArrayList<NavDrawerItem> navDrawerItems) {
        this.navDrawerItems = navDrawerItems;
    }

    public NavDrawerListAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(NavDrawerListAdapter adapter) {
        this.adapter = adapter;
    }

    public ListView getmDrawerList() {
        return mDrawerList;
    }

    public void setmDrawerList(ListView mDrawerList) {
        this.mDrawerList = mDrawerList;
    }

    public MainActivity getHomePage() {
        return homePage;
    }

    public void setHomePage(MainActivity homePage) {
        this.homePage = homePage;
    }









    public DeleteNavigationDrawer(Context context){

            this.context=context;

    }

    public void initialize(){



        homePage = (MainActivity)context;
         mDrawerTitle = homePage.getTitle();



        navMenuTitles = homePage.getResources().getStringArray(R.array.nav_drawer_items);

        navMenuIcons = homePage.getResources()
                .obtainTypedArray(R.array.nav_drawer_icons);

        mDrawerLayout = (DrawerLayout) homePage.findViewById(R.id.drawer_layout);
       // mDrawerList = (ListView) homePage.findViewById(R.id.list_slidermenu);

        navDrawerItems = new ArrayList<NavDrawerItem>();

        //DeleteNavigationDrawer navigationDrawer = new DeleteNavigationDrawer();
        //navigationDrawer.initialize(navMenuTitles,navMenuIcons,mDrawerLayout,mDrawerList,navDrawerItems);
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));

        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
        // Find People
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
        // Photos
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
        // Communities, Will add a counter here
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1), true, "22"));
        // Pages
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
        // What's hot, We  will add a counter here
        //navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1), true, "50+"));

        // load slide menu items


        // Recycle the typed array
        navMenuIcons.recycle();

        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

        // setting the nav drawer list adapter
        adapter = new NavDrawerListAdapter(homePage.getApplicationContext(),
                navDrawerItems);
        mDrawerList.setAdapter(adapter);

        // enabling action bar app icon and behaving it as toggle button
        homePage.getActionBar().setDisplayHomeAsUpEnabled(true);
        homePage.getActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(homePage, mDrawerLayout,
                R.drawable.ic_drawer, //nav menu toggle icon
                R.string.app_name, // nav drawer open - description for accessibility
                R.string.app_name // nav drawer close - description for accessibility
        ) {
            public void onDrawerClosed(View view) {
                homePage.getActionBar().setTitle(homePage.getmTitle());
                // calling onPrepareOptionsMenu() to show action bar icons
                homePage.invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                homePage.getActionBar().setTitle(mDrawerTitle);
                // calling onPrepareOptionsMenu() to hide action bar icons
                homePage.invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        //mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);


    }

    /**
     * Slide menu item click listener
     * */
    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // display view for selected nav drawer item
            displayView(position);
        }
    }

    /*
	 * Diplaying fragment view for selected nav drawer list item
	 * */
    public void displayView(int position) {
        MainActivity homePage = (MainActivity) context;

        // update the main content by replacing fragments
        Fragment fragment = null;
        switch (position) {
            case 1:

                fragment = new MapFragment();
                break;
            case 2:
              fragment = new FindPeopleFragment();
                break;
            case 3:
                fragment = new PhotosFragment();
                break;
            case 4:
                fragment = new CommunityFragment();
                break;
            case 5:
                fragment = new PagesFragment();
                break;


            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = homePage.getSupportFragmentManager();
            //fragmentManager.beginTransaction()
              //      .replace(R.id.frame_container, fragment).commit();

            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            homePage.setTitle(navMenuTitles[position-1]);
            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            // error in creating fragment
            Log.e("HomePageActivity", "Error in creating fragment");
        }
    }



}
