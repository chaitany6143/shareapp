package com.wannashare.utils;

import android.os.Environment;

import java.io.File;
import java.io.IOException;

/**
 * Created by chaitanya on 8/29/15.
 */
public class FileUtil {

    static File sdcard = Environment.getExternalStorageDirectory() ;
    static File appFolder;


    public static boolean createAppDirIfNotExist() {

        appFolder = new File(sdcard.getAbsoluteFile(), "wannashare");

        if (!appFolder.exists()) {
            boolean created = appFolder.mkdirs();
            if (!created) {
                return false;
            }
        }
        return true;


    }
    public static String getAppFolder(){
        if(appFolder==null){
            createAppDirIfNotExist();

        }
        return appFolder.getAbsolutePath();
    }
}
