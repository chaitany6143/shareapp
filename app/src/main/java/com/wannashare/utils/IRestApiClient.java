package com.wannashare.utils;

import com.wannashare.localdatastore.models.Post;
import com.wannashare.localdatastore.models.NearbyPostComment;
import com.wannashare.localdatastore.models.Session;
import com.wannashare.localdatastore.models.User;
import com.wannashare.localdatastore.models.UserPost;
import com.wannashare.localdatastore.models.UserPostLike;

import java.util.List;
import java.util.Set;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.MultipartTypedOutput;

/**
 * Created by chaitanya on 7/1/15.
 */
public interface IRestApiClient {

    @POST("/user")
    void createUser(@Body User user,Callback<String> cb);

    @POST("/user/session")
    void addSession(@Body Session session,Callback<String> cb);

    @GET("/user/{userid}/sessions")
    void getSessions(@Path("userid") String userid,Callback<Set<Session>> cb);

    @GET("/user/{userid}")
    void getUser(@Path("userid") String userid,Callback<User> cb);

    @POST("/user/post")
    void addPost(@Body Post post,Callback<String> cb);

    @GET("/user/{userid}/posts")
    void getPosts(@Path("userid") String userid,Callback<Set<String>> cb);

    @GET("/user/nearby/posts")
    void getNearbyPosts(Callback<List<UserPost>> cb);

    @POST("/user/{userid}/post/{postid}/images")
    String addUserPostImage(@Path("userid") String userid,@Path("postid") String postid,@Body MultipartTypedOutput files);

    @GET("/user/post/{postid}/comments")
    void getComments(@Path("postid") String postid,Callback<List<NearbyPostComment>> cb);


    @GET("/images")
    @Headers({"Content-Type: image/jpeg"})
    Response getImages(@Query("fileLocation") String fileLocation);

    @GET("/user/post/{postid}")
    UserPost getUserPostById(@Path("postid") String postId);

    @POST("/user/post/comment")
    void savePostComment(@Body NearbyPostComment comment,Callback<String> cb);

    @POST("/user/post/like")
    void savePostAction(@Body UserPostLike like,Callback<Boolean> cb);
}
