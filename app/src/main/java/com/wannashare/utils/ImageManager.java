package com.wannashare.utils;

import android.graphics.Bitmap;
import android.os.Environment;
import com.wannashare.datahelper.async.*;
import com.wannashare.localdatastore.models.UserPost;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class ImageManager {


    public static String saveToSdCard(Bitmap bitmap, String relativeLocation,String fileName) {

        String stored = null;

        File sdcard = Environment.getExternalStorageDirectory() ;

     //   FileUtil.createAppDirIfNotExist();


        File destinationDir = new File(FileUtil.getAppFolder()+"/"+relativeLocation);
        if(!destinationDir.exists()) {
            boolean created = destinationDir.mkdirs();
            if (!created) {
                try {
                    throw new IOException("Unable to create destination dir " + destinationDir);
                } catch (IOException e) {

                }
            }
        }
            File file = new File(destinationDir+"/"+fileName) ;
        if (!file.exists()) {

            try {
                FileOutputStream out = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();
                stored = "success";
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
            else{
        stored = "Already exists";
        }

        return stored;
    }

    public static boolean isImagePresent(String relativeLocation,String fileName) {

        return new File(FileUtil.getAppFolder()+relativeLocation+"/"+fileName).exists();


    }
    public static void downloadIfNotExist(UserPost userPost) {
        Bitmap b = null;
        List<String> imagesLocation = userPost.getThumbnaillocation();
        for (String imageLocation : imagesLocation) {

            String relativeLocation = "/" + userPost.getUser().getUserid() + "/" + userPost.getPost().getPostid()+"/thumbnail";
            String fileName = imageLocation.substring(imageLocation.lastIndexOf("/") + 1);
            if (!ImageManager.isImagePresent(relativeLocation, fileName)) {

                new GetImages(imageLocation, userPost).execute();
            }


        }
    }}