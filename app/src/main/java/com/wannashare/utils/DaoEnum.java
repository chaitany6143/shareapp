package com.wannashare.utils;

/**
 * Created by chaitanya on 8/10/15.
 */
public enum DaoEnum {

    SESSION, NEARBYPOST, NEARBYPOSTIMAGE, POST, NEARBYPOSTCOMMENT, USER
}