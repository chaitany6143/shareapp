package com.wannashare.utils;

/**
 * Created by chaitanya on 8/24/15.
 */
public class Utils {

    public static int getCurrentLength(String[] imagesPath) {
        int result = 0;
        for(int i=0;i<imagesPath.length;i++){
            if(imagesPath[i]!=null)
                result++;
        }
        return result;
    }
}
