package com.wannashare.application;

import android.app.Application;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.text.TextUtils;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.facebook.FacebookSdk;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wannashare.datahelper.async.BackGroundService;
import com.wannashare.wrappers.LruBitmapCache;
import com.wannashare.datahelper.user.UserDataHelper;
import com.wannashare.utils.FragmentEnum;
import com.wannashare.wrappers.ContextStore;
import com.wannashare.localdatastore.LocalCacheAccessor;
import com.wannashare.utils.Constants;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by chaitanya on 5/17/15.
 */
public class MainApplication extends Application {

    private Map<FragmentEnum,Fragment> activeFragmentMap = new HashMap<>();


    Intent commentIntent = new Intent();

    public static final String TAG = MainApplication.class.getSimpleName();


    UserDataHelper userDataHelper;

    RestAdapter restAdapter;

    LocalCacheAccessor localCacheAccessor;


    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    LruBitmapCache mLruBitmapCache;
    Gson gson;




    @Override
    public void onCreate() {


        super.onCreate();

        ContextStore.getInstance().setApplicationContext(this);



        FacebookSdk.sdkInitialize(getApplicationContext());

         gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .create();


        restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.BASE_URL)
                .setConverter(new GsonConverter(gson))
                .build();

        localCacheAccessor = new LocalCacheAccessor(this);

        localCacheAccessor.persistToCache(Constants.SEARCH_DISTANCE, Float.valueOf(Constants.DEFAULT_SEARCH_DISTANCE).toString());


        try {



            userDataHelper= new UserDataHelper();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        startService(new Intent(this, BackGroundService.class));









       /* try {
            List<User> userList = restService.getUsers();
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
        }*/


    }



    public UserDataHelper getUserDataHelper(){
        return userDataHelper;
    }

    public RestAdapter getRestAdapter(){

        return restAdapter;
    }
    public LocalCacheAccessor getLocalCacheAccessor(){

        return localCacheAccessor;
    }


    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            getLruBitmapCache();
            mImageLoader = new ImageLoader(this.mRequestQueue, mLruBitmapCache);
        }

        return this.mImageLoader;
    }

    public LruBitmapCache getLruBitmapCache() {
        if (mLruBitmapCache == null)
            mLruBitmapCache = new LruBitmapCache();
        return this.mLruBitmapCache;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public void addToActiveFragmentMap(FragmentEnum fragmentEnum,Fragment value){
        activeFragmentMap.put(fragmentEnum,value);
    }
    public Fragment getActiveFragment(FragmentEnum key){
        return activeFragmentMap.get(key);
    }
    public void removeFromActiveFragmentMap(FragmentEnum key){
        activeFragmentMap.remove(key);
    }



    /*public RestService getRestService(){
        return RestService.getInstance();
    }*/
    public Gson getGson() {
        return gson;
    }

}
