package com.wannashare.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wannashare.R;
import com.wannashare.datahelper.user.UserDataHelper;
import com.wannashare.localdatastore.models.NearbyPostComment;
import com.wannashare.localdatastore.models.User;
import com.wannashare.utils.DaoEnum;
import com.wannashare.views.ProfilePictureView;

import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by chaitanya on 8/9/15.
 */
public class CommentListAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private List<NearbyPostComment> commentList;
    UserDataHelper userDataHelper;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    TextView name;
    TextView timestamp;
    TextView statusMsg;
    ProfilePictureView profilePictureView;

    public CommentListAdapter(Activity activity, List<NearbyPostComment> commentList,UserDataHelper userDataHelper) {
        this.activity = activity;
        this.commentList = commentList;
        this.userDataHelper = userDataHelper;
    }

    @Override
    public int getCount() {
        return commentList.size();
    }

    @Override
    public Object getItem(int location) {
        return commentList.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        NearbyPostComment nearbyPostComment = null;
        User userInfo = null;
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.comment_item, null);


         name = (TextView) convertView.findViewById(R.id.name);
         timestamp = (TextView) convertView
                .findViewById(R.id.timestamp);
         statusMsg = (TextView) convertView
                .findViewById(R.id.commentText);

         profilePictureView = (ProfilePictureView) convertView.findViewById(R.id.profilePic);


        // NetworkImageView profilePic = (NetworkImageView) convertView
        //       .findViewById(R.id.profilePic);
        //FeedImageView feedImageView = (FeedImageView) convertView
        //      .findViewById(R.id.feedImages);

        nearbyPostComment = commentList.get(position);

        try {
             userInfo  = (User) userDataHelper.getFromLocalDbByField("userid", nearbyPostComment.getUserid(), DaoEnum.USER).get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        name.setText(userInfo.getName());
        Date date = new Date(Long.parseLong(nearbyPostComment.getTimestamp()));

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy','HH:mm:ss");

        String dateString = simpleDateFormat.format(date);
        timestamp.setText(dateString);
        statusMsg.setText(nearbyPostComment.getComment());
        profilePictureView.setProfileId(userInfo.getProfileid());


        return convertView;
    }

}


