package com.wannashare.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by chaitanya on 8/20/15.
 */
public class ImagesGalleryAdapter extends PagerAdapter {

    private Context ctx;
    private String[] dataList;
    private Bitmap yourbitmap;

    public ImagesGalleryAdapter(Context act, String[] lista) {

        ctx = act;
        dataList = lista;
    }

    public int getCount() {
        return dataList.length;
    }

    public Object getItem(int pos) {
        return pos;
    }

    @Override
    public Object instantiateItem(View collection, int pos) {



            //imagesPathList.add(imagesPath[i]);
            yourbitmap = BitmapFactory.decodeFile(dataList[pos]);
            //resized=    Bitmap.createScaledBitmap(yourbitmap, 200,300, true);
            ImageView iv = new ImageView(ctx);

            iv.setImageBitmap(yourbitmap);
            iv.setAdjustViewBounds(true);
            ((ViewPager) collection).addView(iv);
            // gallery.addView(iv);





        return iv;
    }

    @Override
    public void destroyItem(View collection, int position, Object view) {
        ((ViewPager) collection).removeView((ImageView) view);

    }

    public long getItemId(int pos) {
        return pos;
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        // TODO Auto-generated method stub
        return view == ((ImageView) object);
    }
}