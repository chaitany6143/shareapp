package com.wannashare.adapters;



import java.sql.SQLException;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.wannashare.R;
import com.wannashare.application.MainApplication;
import com.wannashare.localdatastore.models.User;
import com.wannashare.models.NavDrawerItem;
import com.wannashare.utils.DaoEnum;
import com.wannashare.views.ProfilePictureView;
import com.wannashare.datahelper.user.UserDataHelper;
import com.wannashare.wrappers.ContextStore;


public class NavDrawerListAdapter extends BaseAdapter {
	
	private Context context;
	private List<NavDrawerItem> navDrawerItems;
	
	public NavDrawerListAdapter(Context context, List<NavDrawerItem> navDrawerItems){
		this.context = context;
		this.navDrawerItems = navDrawerItems;
	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {		
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
	if(position==0)
				convertView = mInflater.inflate(R.layout.drawerlist_first_item, null);
			else
				convertView = mInflater.inflate(R.layout.drawer_list_item, null);

        }

			if(position==0) {
				ProfilePictureView profilePicture = (ProfilePictureView) convertView.findViewById(R.id.profilePicture);

				//profilePicture= (ProfilePictureView) homePage.findViewById(R.id.profilePicture);
				TextView name = (TextView) convertView.findViewById(R.id.name);

				TextView location = (TextView) convertView.findViewById(R.id.location);

				UserDataHelper userDataHelper = ((MainApplication) ContextStore.getApplicationContext()).getUserDataHelper();

				//String userJson = null;
				User user = null;
				try {
					if (userDataHelper.getFromLocalDbByField("userid",AccessToken.getCurrentAccessToken().getUserId(), DaoEnum.USER) != null) {
                        user = (User)userDataHelper.getFromLocalDbByField("userid", AccessToken.getCurrentAccessToken().getUserId(), DaoEnum.USER).get(0);
                        // user = new Gson().fromJson(userJson, User.class);
                    }
                    else{
                        user = userDataHelper.getUserDetailsFromFB();
                    }
				} catch (SQLException e) {
					e.printStackTrace();
				}
				name.setText(user.getName());
				location.setText(user.getLocation());
				profilePicture.setProfileId(user.getProfileid());

				//UserProfileTemp profile = new UserProfileTemp();
				//profile.updateUserProfile(convertView);
			}
			else {

				ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
				TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
				TextView txtCount = (TextView) convertView.findViewById(R.id.counter);

				imgIcon.setImageResource(navDrawerItems.get(position).getIcon());
				txtTitle.setText(navDrawerItems.get(position).getTitle());

				// displaying count
				// check whether it set visible or not
				if (navDrawerItems.get(position).getCounterVisibility()) {
					txtCount.setText(navDrawerItems.get(position).getCount());
				} else {
					// hide the counter view
					txtCount.setVisibility(View.GONE);
				}
			}
        
        return convertView;
	}

}
