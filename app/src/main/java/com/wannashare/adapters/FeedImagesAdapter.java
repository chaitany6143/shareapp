package com.wannashare.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;


import com.wannashare.application.MainApplication;
import com.wannashare.datahelper.user.UserDataHelper;
import com.wannashare.localdatastore.models.NearbyPost;
import com.wannashare.localdatastore.models.NearbyPostImage;
import com.wannashare.localdatastore.models.UserPost;
import com.wannashare.utils.DaoEnum;
import com.wannashare.utils.FileUtil;
import com.wannashare.wrappers.ContextStore;
import com.wannashare.wrappers.LruBitmapCache;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by chaitanya on 8/28/15.
 */


public class FeedImagesAdapter extends BaseAdapter {

    private Context ctx;
    private List<NearbyPostImage> nearbyPostImages;
    private Bitmap yourbitmap;
    LruBitmapCache imageCache = ((MainApplication) ContextStore.getApplicationContext()).getLruBitmapCache();
    String userId;

    public FeedImagesAdapter(Context act, List<NearbyPostImage> lista,String userId) {

        ctx = act;
        this.nearbyPostImages = lista;
        this.userId=userId;
    }

    public int getCount() {
        return Integer.parseInt(String.valueOf(nearbyPostImages.size()));
    }

    public Object getItem(int pos) {
        return pos;
    }




    public long getItemId(int pos) {
        return pos;
    }

    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * {@link LayoutInflater#inflate(int, ViewGroup, boolean)}
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position    The position of the item within the adapter's data set of the item whose view
     *                    we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     *                    is non-null and of an appropriate type before using. If it is not possible to convert
     *                    this view to display the correct data, this method can create a new view.
     *                    Heterogeneous lists can specify their number of view types, so that this View is
     *                    always of the right type (see {@link #getViewTypeCount()} and
     *                    {@link #getItemViewType(int)}).
     * @param parent      The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {



        ImageView iv;

        if (convertView == null) {
             iv = new ImageView(ctx);

            if(nearbyPostImages.get(position).getThumbnailLocation()!=null && nearbyPostImages.get(position).getActuallocation()!=null) {
                String imageLocation = nearbyPostImages.get(position).getThumbnailLocation();
                if(imageCache.get(FileUtil.getAppFolder() + "/" + userId + "/" +
                        nearbyPostImages.get(position).getPostid() + "/thumbnail/" + imageLocation.substring(imageLocation.lastIndexOf("/") + 1))!=null){

                    iv.setImageBitmap(imageCache.getBitmap(FileUtil.getAppFolder() + "/" + userId + "/" +
                            nearbyPostImages.get(position).getPostid() + "/thumbnail/" + imageLocation.substring(imageLocation.lastIndexOf("/") + 1)));
                    iv.setAdjustViewBounds(true);
                }
                else {

                    File file = new File(FileUtil.getAppFolder() + "/" + userId + "/" +
                            nearbyPostImages.get(position).getPostid() + "/thumbnail/" + imageLocation.substring(imageLocation.lastIndexOf("/") + 1));

                    if (file.exists()) {
                        yourbitmap = BitmapFactory.decodeFile(FileUtil.getAppFolder() + "/" + userId + "/" +
                                nearbyPostImages.get(position).getPostid() + "/thumbnail/" + imageLocation.substring(imageLocation.lastIndexOf("/") + 1));
                    }
                    iv.setImageBitmap(yourbitmap);
                    iv.setAdjustViewBounds(true);
                    imageCache.put(FileUtil.getAppFolder() + "/" + userId + "/" +
                            nearbyPostImages.get(position).getPostid() + "/thumbnail/" + imageLocation.substring(imageLocation.lastIndexOf("/") + 1),yourbitmap);
                }
            }
        }
        else{
            iv = (ImageView) convertView;
        }



        return iv;
    }


}