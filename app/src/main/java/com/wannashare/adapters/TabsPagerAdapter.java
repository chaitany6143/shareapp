package com.wannashare.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.wannashare.application.MainApplication;
import com.wannashare.fragments.MapFragment;
import com.wannashare.fragments.UserFeedFragment;
import com.wannashare.utils.Constants;
import com.wannashare.utils.FragmentEnum;
import com.wannashare.wrappers.ContextStore;

/**
 * Created by chaitanya on 9/5/15.
 */
public class TabsPagerAdapter extends FragmentPagerAdapter {

    Fragment fragment = null;
    public TabsPagerAdapter(FragmentManager supportFragmentManager) {
        super(supportFragmentManager);
    }

    /**
     * Return the Fragment associated with a specified position.
     *
     * @param position
     */
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                // Top Rated fragment activity
                fragment =  new MapFragment();
                ((MainApplication)ContextStore.getApplicationContext()).addToActiveFragmentMap(FragmentEnum.MAP,fragment);
                break;
            case 1:
                // Games fragment activity
                fragment =  new UserFeedFragment();
                ((MainApplication)ContextStore.getApplicationContext()).addToActiveFragmentMap(FragmentEnum.USERFEED,fragment);
                break;
        }


        return fragment;
    }


    /**
     * Return the number of views available.
     */
    @Override
    public int getCount() {
        return Constants.tabs.length;
    }
}
