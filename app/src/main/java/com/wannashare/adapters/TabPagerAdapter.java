package com.wannashare.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


import com.wannashare.R;
import com.wannashare.fragments.MapFragment;
import com.wannashare.fragments.WhatsHotFragment;
import com.wannashare.utils.Constants;

import java.util.HashMap;
import java.util.Map;


public class TabPagerAdapter extends FragmentPagerAdapter {


    Fragment fragment ;

    private Map<Integer, Fragment> map = null;

    public static final String[] TABS = Constants.TABS;

    public TabPagerAdapter(FragmentManager fm) {
        super(fm);
        map = new HashMap<>();
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        String tab = "";
        int colorResId = 0;
        switch (position) {

            case 1:
                tab = "User feed";
                colorResId = R.color.white;
                fragment = new WhatsHotFragment();
                break;
            case 0:
            default:
                tab = "Map";
                colorResId = R.color.white;
                fragment = new MapFragment();
                break;

        }
        bundle.putString("tab", tab);
        bundle.putInt("color", colorResId);
        fragment.setArguments(bundle);
        map.put(position, fragment);
        return fragment;
    }

    @Override
    public int getCount() {
        return TABS.length;
    }




    @Override
    public CharSequence getPageTitle(int position) {
        return TABS[position];
    }

}