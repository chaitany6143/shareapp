package com.wannashare.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.wannashare.R;
import com.wannashare.activities.CommentActivity;
import com.wannashare.activities.MainActivity;
import com.wannashare.application.MainApplication;
import com.wannashare.datahelper.async.post.GetFeedItemData;
import com.wannashare.datahelper.async.post.SaveActionData;
import com.wannashare.datahelper.user.UserDataHelper;
import com.wannashare.fragments.PostCommentFragment;
import com.wannashare.localdatastore.models.NearbyPost;
import com.wannashare.localdatastore.models.NearbyPostImage;
import com.wannashare.localdatastore.models.User;
import com.wannashare.localdatastore.models.UserPost;
import com.wannashare.localdatastore.models.UserPostLike;
import com.wannashare.utils.DaoEnum;
import com.wannashare.views.ProfilePictureView;
import com.wannashare.wrappers.ContextStore;
import com.wannashare.wrappers.LruBitmapCache;

import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by chaitanya on 8/9/15.
 */
public class FeedListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<NearbyPost> nearbyPosts;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    boolean hasLiked = false;
    UserDataHelper userDataHelper;

   // GridView gallery;
   // TextView name;
   // TextView timestamp;
   // TextView statusMsg;
   // ProfilePictureView profilePictureView;
   // TextView likeButton;
   // TextView dislikeButton;
   // TextView commentButton;
   // TextView likes;
   // TextView dislikes;
   // TextView comments;


    public FeedListAdapter(Activity activity, List<NearbyPost> feedItems) {
        this.activity = activity;
        this.nearbyPosts = feedItems;
        try {
            userDataHelper = new UserDataHelper();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getCount() {
        return nearbyPosts.size();
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     *                 data set.
     * @return The data at the specified position.
     */
    @Override
    public Object getItem(int position) {
        return null;
    }

    /*@Override
    public Object getItem(int location) {
        return nearbyPosts.get(location);
    }
*/
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder mHolder;
        String tempString = null;
        NearbyPost nearbyPost = null;
        User userInfo = null;
        List<NearbyPostImage> nearbyPostImages = null;
        nearbyPost = nearbyPosts.get(position);
        if (convertView == null) {


            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.feed_item, null);

            mHolder = new ViewHolder();
            mHolder.name = (TextView) convertView.findViewById(R.id.name);
            mHolder.timestamp = (TextView) convertView
                    .findViewById(R.id.timestamp);
            mHolder.statusMsg = (TextView) convertView
                    .findViewById(R.id.txtStatusMsg);

            mHolder.profilePictureView = (ProfilePictureView) convertView.findViewById(R.id.profilePic);
            mHolder.gallery = (GridView) convertView.findViewById(R.id.photogridview);

            mHolder.likeButton = (TextView) convertView.findViewById(R.id.like);
            mHolder.dislikeButton = (TextView) convertView.findViewById(R.id.dislike);
            mHolder.commentButton = (TextView) convertView.findViewById(R.id.comment);

            /* set total likes,dislikes,comments */
            mHolder.likes = (TextView) convertView.findViewById(R.id.totallikes);
            mHolder.dislikes = (TextView) convertView.findViewById(R.id.totaldislikes);
            mHolder.comments = (TextView) convertView.findViewById(R.id.totalcomments);

            mHolder.commentButton.setTag(R.id.comment, nearbyPost.getPostid());
            mHolder.comments.setTag(R.id.totalcomments, nearbyPost.getPostid());






            convertView.setTag(mHolder);

        } else {
            mHolder = (ViewHolder) convertView.getTag();

        }
        if(userDataHelper.getFromLocalCache(String.valueOf(nearbyPost.getPostid()))!=null) {
            if (userDataHelper.getFromLocalCache(String.valueOf(nearbyPost.getPostid())).equals("1"))
                mHolder.likeButton.setTextColor(Color.parseColor("#41CD8C"));

             else
                mHolder.dislikeButton.setTextColor(Color.parseColor("#E91E63"));

        }

           // tempString = mHolder.likes.getText().toString();

            mHolder.likes.setText(nearbyPost.getTotallikes() + " Cools");


            //tempString = mHolder.dislikes.getText().toString();
            mHolder.dislikes.setText(nearbyPost.getTotaldislikes() + " Mehs");


            //tempString = mHolder.comments.getText().toString();
            mHolder.comments.setText(nearbyPost.getTotalcomments() + " Comments");


        final Animation mAnimation = new AlphaAnimation(1, 0);
        mAnimation.setDuration(500);
        mAnimation.setInterpolator(new LinearInterpolator());
        // mAnimation.setRepeatCount(Animation.INFINITE);
        mAnimation.setRepeatMode(Animation.REVERSE);

        final NearbyPost finalNearbyPost = nearbyPost;
        mHolder.likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHolder.likeButton.startAnimation(mAnimation);
                mHolder.likeButton.clearAnimation();
                ((TextView) v).setTextColor(Color.parseColor("#41CD8C"));
                mHolder.likes.setText(finalNearbyPost.getTotallikes() + 1 + " Cools");
                UserPostLike userPostLike = new UserPostLike();
                userPostLike.setPostid(finalNearbyPost.getPostid());
                userPostLike.setUserid(finalNearbyPost.getUserid());
                userPostLike.setAction(1);

                    new SaveActionData(userPostLike).execute();

                hasLiked=true;
            }
        });
        mHolder.dislikeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!hasLiked) {
                    ((TextView) v).setTextColor(Color.parseColor("#E91E63"));
                    mHolder.dislikes.setText(finalNearbyPost.getTotaldislikes() + 1 + " Mehs");
                    UserPostLike userPostLike = new UserPostLike();
                    userPostLike.setPostid(finalNearbyPost.getPostid());
                    userPostLike.setUserid(finalNearbyPost.getUserid());
                    userPostLike.setAction(0);
                    new SaveActionData(userPostLike).execute();
                }
            }
        });

        mHolder.commentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String postid = v.getTag(R.id.comment).toString();
                Intent intent = new Intent(activity, CommentActivity.class);
                intent.putExtra("postid", postid);
                activity.startActivity(intent);
               /* PostCommentFragment postCommentFragment = new PostCommentFragment();
                Bundle args = new Bundle();
                args.putString("postid", postid);
                postCommentFragment.setArguments(args);
                FragmentManager fragmentManager = ((MainActivity) activity).getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, postCommentFragment).commit();  */
            }
        });
        mHolder.comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String postid = v.getTag(R.id.totalcomments).toString();
                Intent intent = new Intent(activity, CommentActivity.class);
                intent.putExtra("postid", postid);
                activity.startActivity(intent);
            }
        });


        try {
            new GetFeedItemData(mHolder,nearbyPost,activity).execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
           // mHolder.commentButton.setTag(R.id.comment, nearbyPost.getPostid());









        return convertView;

     /*   if (convertView != null)
            return convertView;
        else {
            String tempString = null;
            NearbyPost nearbyPost = null;
            User userInfo = null;
            List<NearbyPostImage> nearbyPostImages = null;
            nearbyPost = nearbyPosts.get(position);

            if (inflater == null)
                inflater = (LayoutInflater) activity
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.feed_item, null);

            name = (TextView) convertView.findViewById(R.id.name);
            timestamp = (TextView) convertView
                    .findViewById(R.id.timestamp);
            statusMsg = (TextView) convertView
                    .findViewById(R.id.txtStatusMsg);

            profilePictureView = (ProfilePictureView) convertView.findViewById(R.id.profilePic);
            gallery = (GridView) convertView.findViewById(R.id.photogridview);

            likeButton = (TextView) convertView.findViewById(R.id.like);
            dislikeButton = (TextView) convertView.findViewById(R.id.dislike);
            commentButton = (TextView) convertView.findViewById(R.id.comment);

            *//* set total likes,dislikes,comments *//*
            likes = (TextView) convertView.findViewById(R.id.totallikes);
            dislikes = (TextView) convertView.findViewById(R.id.totaldislikes);
            comments = (TextView) convertView.findViewById(R.id.totalcomments);
            tempString = likes.getText().toString();
            likes.setText(nearbyPost.getTotallikes() + " " + tempString);
            tempString = dislikes.getText().toString();
            dislikes.setText(nearbyPost.getTotaldislikes() + " " + tempString);
            tempString = comments.getText().toString();
            comments.setText(nearbyPost.getTotalcomments() + " " + tempString);

            *//*end *//*
            commentButton.setTag(R.id.comment, nearbyPost.getPostid());
            final Animation mAnimation = new AlphaAnimation(1, 0);
            mAnimation.setDuration(500);
            mAnimation.setInterpolator(new LinearInterpolator());
            // mAnimation.setRepeatCount(Animation.INFINITE);
            mAnimation.setRepeatMode(Animation.REVERSE);

            likeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    likeButton.startAnimation(mAnimation);
                    likeButton.clearAnimation();
                    ((TextView) v).setTextColor(Color.parseColor("#41CD8C"));
                }
            });
            dislikeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((TextView) v).setTextColor(Color.parseColor("#E91E63"));
                }
            });

            commentButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String postid = v.getTag(R.id.comment).toString();
                    Intent intent = new Intent(activity, CommentActivity.class);
                    intent.putExtra("postid", postid);
                    activity.startActivity(intent);
                    *//*PostCommentFragment postCommentFragment = new PostCommentFragment();
                    Bundle args = new Bundle();
                    args.putString("postid", postid);
                    postCommentFragment.setArguments(args);
                    FragmentManager fragmentManager = ((MainActivity) activity).getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragment_container, postCommentFragment).commit();*//*
                }
            });

            *//*Get the user information from the local db*//*
            try {
                UserDataHelper userDataHelper = new UserDataHelper();
                userInfo = (User) (userDataHelper.getFromLocalDbByField("userid", nearbyPost.getUserid(), DaoEnum.USER)).get(0);
                nearbyPostImages = (List<NearbyPostImage>) userDataHelper.getFromLocalDbByField("postid", String.valueOf(nearbyPost.getPostid()), DaoEnum.NEARBYPOSTIMAGE);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            name.setText(userInfo.getName());
            Date date = new Date(Long.parseLong(nearbyPost.getTimestamp()));

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy','HH:mm:ss");

            String dateString = simpleDateFormat.format(date);
            timestamp.setText(dateString);
            statusMsg.setText(nearbyPost.getPost());

            profilePictureView.setProfileId(userInfo.getProfileid());

            gallery.setAdapter(new FeedImagesAdapter(activity, nearbyPostImages));


            return convertView;
        }*/
    }

    public class ViewHolder {
        public GridView gallery;
        public TextView name;
        public TextView timestamp;
        public TextView statusMsg;
        public ProfilePictureView profilePictureView;
        public TextView likeButton;
        public TextView dislikeButton;
        public TextView commentButton;
        public TextView likes;
        public TextView dislikes;
        public TextView comments;

    }

}


