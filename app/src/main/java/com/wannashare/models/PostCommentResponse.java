package com.wannashare.models;


import com.wannashare.localdatastore.models.NearbyPostComment;

public class PostCommentResponse {
	
	NearbyPostComment nearbyPostComment;
	User user;
	public NearbyPostComment getNearbyPostComment() {
		return nearbyPostComment;
	}
	public void setNearbyPostComment(NearbyPostComment nearbyPostComment) {
		this.nearbyPostComment = nearbyPostComment;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
}
