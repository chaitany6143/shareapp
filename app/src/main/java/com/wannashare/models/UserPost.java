package com.wannashare.models;

import java.util.List;

/**
 * Created by chaitanya on 8/5/15.
 */
public class UserPost {
    User user;
    Post post;
    List<String> imagesLocation;

    public List<String> getImagesLocation() {
        return imagesLocation;
    }

    public void setImagesLocation(List<String> imagesLocation) {
        this.imagesLocation = imagesLocation;
    }





    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
    public Post getPost() {
        return post;
    }
    public void setPost(Post post) {
        this.post = post;
    }
}
